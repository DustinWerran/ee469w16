vlib work

vlog -work work ../3to8Decoder.v

vsim -t 1pS -novopt Decoder3to8_tb

view signals
view wave

do wave_3to8Decoder.do

run -all
