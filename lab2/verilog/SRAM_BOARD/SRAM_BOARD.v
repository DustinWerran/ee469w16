//`timescale 1ps/1ps

//`include "SRAM32.v"

module SRAM_BOARD(CLOCK_50, LEDR, KEY);
	input CLOCK_50;
	input [3:0] KEY;
	
	//---OUTPUT-----//
	output [9:0] LEDR;
	//-----WIRES-------//
	wire [31:0] DATA;
	wire [9:0] LED;
	wire [31:0] LED_DATA;
	wire WE, OE;
	wire [10:0] ADDRESS;
	//======registers======//
	reg [24:0] tbase;
	
	assign LEDR[7:0] = LED_DATA[7:0];
	assign LEDR[8] = WE;
	assign RST = !KEY[0];
	assign LEDR[9] = OE;
	assign LED_DATA = DATA;
	
	always@(posedge CLOCK_50 or posedge RST) begin
		if(RST) begin
			tbase <= 0;
		end else begin
			tbase <= tbase + 1;
		end
	end
	
	SRAM32 mySRAM (.ADDRESS(ADDRESS), .CLK(tbase[22]), .WE(WE), .OE(OE), .RST(RST), .DATA(DATA));
	driver mydriver (.ADDRESS(ADDRESS), .CLK(tbase[22]), .WE(WE), .OE(OE), .RST(RST), .DATA(DATA));

endmodule

module driver(DATA, ADDRESS, CLK, WE, OE, RST);
	
	//--------INPUTS-----------//
	input CLK, RST;
	
	//---------OUTPUTS---------//
	inout [31:0] DATA;
	output [10:0] ADDRESS;
	output WE, OE;
	
	//------REGISTERS--------//
	reg [31:0] DATA_VAL;
	reg [31:0] DATAOUT, populate_SRAM;
	reg WE, OE, go, write, read;
	reg [10:0] ADDRESS;
	
	//------ASSIGNMENTS------//
	assign DATA = OE ? 32'bz : DATAOUT;
	
	always@(posedge CLK or posedge RST) begin
		if(RST) begin
			WE <= 1'b0;
			OE <= 1'b0;
			ADDRESS <= 11'd0;
			
			read <= 1'b0;
			populate_SRAM <= 32'd0;
		end 
		else begin
			if (populate_SRAM < 32'd256) begin
				WE <= populate_SRAM[0];
				ADDRESS <= {populate_SRAM[7:0]};
				if (populate_SRAM[0]) begin
					DATAOUT <= {24'b0, ~populate_SRAM[7:1]};
				end
				
				if (populate_SRAM == 255) begin
					read <= 1'b1;
					ADDRESS <= 32'd257;
					WE <= 0;
				end
				populate_SRAM <= populate_SRAM + 1;
			end
			if(read) begin
				OE <= 1'b1;
				ADDRESS <= ADDRESS - 2;
				if(ADDRESS == 0) begin
					read <= 0;
				end
			end
		end
	end
endmodule

module testbench_SRAM_board;
		wire [9:0] LEDR;
		reg [3:0] KEY;
		reg CLK;

		SRAM_BOARD dut (.CLOCK_50(CLK), .LEDR(LEDR), .KEY(KEY));
		
		always begin
			#50 CLK <= ~CLK;
		end 
		
		initial 
		begin
//			$dumpfile("DRIVER_TEST.vcd");
//			$dumpvars(0, mydriver, mySRAM);
//			$monitor(RST, DATA, CLK, WE, OE);
			CLK <= 1'b0;
			KEY[0] <= 1'b0;
			KEY[3:1] <= 3'b0;
			@(posedge CLK);
			KEY[0] <= 1'b1;
			repeat (1000) @(posedge CLK);
			$stop;
		end	
endmodule