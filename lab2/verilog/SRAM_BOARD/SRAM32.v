module SRAM32(DATA, ADDRESS, CLK, WE, OE, RST);
	//----BI-DIRECTIONAL PORT----//
	inout [31:0] DATA;
	
	//---------INPUTS---------//
	
	input [10:0] ADDRESS;
	input CLK, WE, OE, RST;
	
	//---------REGISTERS-----------//
	reg [10:0] ADDRreg;
	reg [31:0] DATAreg, DATAOUT;
	reg load;
	//-----MEMORY REGISTER---------//
	reg [15:0] MEMORY [0:2023];
	//-------INTEGERS----------//
	integer i;
	
	assign DATA = OE ? DATAOUT : 32'bz;
	
	always@(posedge CLK) begin
	
		//----RESET STATE-------//
		if(RST) begin
			ADDRreg <= 0;
			DATAreg <= 0;
			load <= 0;
		end else begin
			//-----WRITING----------//
			if(WE & !OE) begin
				ADDRreg <= ADDRESS;
				DATAreg <= DATA;
				load <= 1;
			end
			
			if(load) begin
				{MEMORY[ADDRreg], MEMORY[ADDRreg + 1]} <= DATAreg;
				load <= 0;
			end
			//-------READING--------//
			if(!WE & OE) begin
				DATAOUT <= {MEMORY[ADDRESS], MEMORY[ADDRESS + 1]}; 
			end
		end
	end
	
endmodule