onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /testbench_SRAM_board/dut/CLOCK_50
add wave -noupdate {/testbench_SRAM_board/dut/tbase[0]}
add wave -noupdate /testbench_SRAM_board/dut/KEY
add wave -noupdate /testbench_SRAM_board/dut/LEDR
add wave -noupdate -radix decimal /testbench_SRAM_board/dut/DATA
add wave -noupdate -radix decimal /testbench_SRAM_board/dut/ADDRESS
add wave -noupdate /testbench_SRAM_board/dut/WE
add wave -noupdate /testbench_SRAM_board/dut/OE
add wave -noupdate /testbench_SRAM_board/dut/RST
add wave -noupdate -radix decimal /testbench_SRAM_board/dut/mydriver/populate_SRAM
add wave -noupdate /testbench_SRAM_board/dut/mydriver/read
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {98750 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 281
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {98491 ps} {100133 ps}
