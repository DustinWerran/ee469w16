vlib work

vlog -work work ./SRAM_BOARD.v

vsim -t 1ps -novopt testbench_SRAM_board

view signals
view wave

do wave_SRAM_BOARD.do

run -all
