// this module used with permission as provided in
// the specification document for EE469 Lab 1,
// modified to be active high reset and to have
// an enable pin

module DFlipFlop(q, qBar, D, en, clk, rst); 

input D, en, clk, rst; 
output q, qBar; 

reg q; 

not n1 (qBar, q); 

always@ (posedge rst or posedge clk) begin 

	if(rst) q = 0; 
	else if (en) q = D; 
	
end 

endmodule 