module IntegrationTest (
	output [7:0] LEDR,
	input [1:0] KEY,
	input CLOCK_50
);

wire reset;

reg Write;
reg WriteDone;
reg DoneWriting;
reg WriteToReg;
reg DoneWriteToReg1;
reg DoneReadFromReg;
reg DoneRegToSRAM;
reg [15:0] WriteData;
reg [10:0] SRAMAddress;
reg [4:0] RegWriteAddress;
reg [23:0] tbase;
reg [4:0] Read1Address;
reg [4:0] Read2Address;
reg [31:0] Reg1ReadData;
reg [31:0] Reg2ReadData;

assign reset = KEY[1];
assign LEDR[7:0] = KEY[0] ? Reg2ReadData[7:0] : Reg1ReadData[7:0];

SRAM_Reg_Integration uut (
	.Reg1ReadData(Reg1ReadData),
	.Reg2ReadData(Reg2ReadData),
	.WriteDone(WriteDone),
	.SRAMAddress(SRAMAddress),
	.RegWriteAddress(RegWriteAddress),
	.WriteData(WriteData),
	.Read1Address(Read1Address),
	.Read2Address(Read2Address),
	.Write(Write),
	.WriteToReg(WriteToReg),
	.clock(tbase[22]),
	.reset(reset)
);

always@(posedge CLOCK_50) tbase <= tbase + 1'b1;

always@(posedge tbase[22]) begin
	if (reset) begin
		WriteData <= 16'd127;
		Write <= 1'b0;
		SRAMAddress <= 11'd0;
		RegWriteAddress <= 5'd0;
		Read1Address <= 5'd31;
		Read2Address <= 5'd31;
		WriteToReg <= 1'b0;
		//Tasks
		DoneReadFromReg <= 1'b1;
		DoneWriteToReg1 <= 1'b1;
		DoneRegToSRAM <= 1'b1;
		DoneWriting <= 1'b0;
	end else begin
		if (!DoneWriting) begin
			Write <= 1'b1;
			// might not write to the first address?
			if (Write) begin
				for(i = 0; i < 128; i = i + 1) begin
					WriteData <= WriteData - 16'd1;
					SRAMAddress <= SRAMAddress + 11'd1;
					while (!WriteDone) begin
						Write <= 1'b1;
					end
				end
				DoneWriting <= 1'b1;
				DoneWriteToReg1 <= 1'b0;
				SRAMAddress <= 11'd0;
				Write <= 1'b0;
			end
		end else if (!DoneWriteToReg1) begin
			WriteToReg <= 1'b1;
			// might not write to the first address?
			if (WriteToReg) begin
				for(i = 0; i < 32; i = i + 1) begin
					RegWriteAddress <= RegWriteAddress + 5'd1;
					SRAMAddress <= SRAMAddress + 11'd1;
				end
				WriteToReg <= 1'b0;
				RegWriteAddress <= 5'd0;
				SRAMAddress <= 11'd0;
				DoneWriteToReg1 <= 1'b1;
				DoneReadFromReg <= 1'b0;
			end
		end else if (!DoneReadFromReg) begin
			Read1Address <= 5'd0;
			Read2Address <= 5'd16;
			if (Read2Address == 5'd16) begin
				for(i = 0; i < 16; i = i + 1) begin
					Read1Address <= Read1Address + 5'd1;
					Read2Address <= Read2Address + 5'd1;
				end
				Read1Address <= 5'd31;
				Read2Address <= 5'd31;
				DoneReadFromReg <= 1'b1;
				DoneRegToSRAM <= 1'b0;
			end
		end else if (!DoneRegToSRAM) begin
			// not sure how to do this one, might have wrong approach
			Write <= 1'b1;
			Read1Address <= 5'd0;
			Read2Address <= 5'd16;
			SRAMAddress <= 11'd128;
			WriteData <= Reg1ReadData[15:0];
			if (Write == 1'b1) begin
				for(i = 0; i < 4; i = i + 1) begin
					if (i == 0) begin
						for(j = 0; j < 16; j = j + 1) begin
							SRAMAddress <= SRAMAddress + 1'd1;
							Read1Address <= Read1Address + 1'd1;
							WriteData <= Reg1ReadData[15:0];
						end
					end else if (i == 1) begin
					end else if (i == 2) begin
					end else begin
					end
				end
			end
		end
end
