// all timing assumed to be two clock cycles to read and write to SRAM
/*`include "../SRAM32.v"
`include "Regfile.v"
`include "DFlipFlop.v"
`include "Decoder5to32.v"
`include "Decoder2to4.v"
`include "Bitcell.v"
`include "3to8Decoder.v"*/

module SRAM_Reg_Integration(
	input CLOCK_50,
	input [1:0] KEY,
	output [7:0] LEDR
);

wire reset;
wire clock;

assign reset = !KEY[1];
assign clock = CLOCK_50;

reg RegWriteEnable;
reg [31:0] WriteRegData;
reg [4:0] RegWriteAddress;
reg [4:0] Read1Address, Read2Address;

reg [31:0] WriteSRAMData;
reg [10:0] SRAMAddress;
reg WriteSRAM, OESRAM;

wire [31:0] Reg1ReadData, Reg2ReadData;

wire [31:0] SRAMData;

reg write_SRAM, read_SRAM;
reg [1:0] block;
reg [8:0] populate_SRAM;
reg [7:0] populate_regfile;
reg [7:0] readout;
reg [7:0] write_block_to_SRAM;
reg [1:0] read_slow;
reg [23:0] slowdown;
reg [1:0] slowcount;
reg [7:0] CLK_VAL;
reg [25:0] NEW_CLOCK;

reg speed_up;

assign LEDR = KEY[0] ? Reg1ReadData[7:0] : Reg2ReadData[7:0];
assign SRAMData = WriteSRAM ? WriteSRAMData : 32'bz;


Regfile registers (
	.Reg1ReadData(Reg1ReadData),
	.Reg2ReadData(Reg2ReadData),
	.Reg1ReadSelect(Read1Address),
	.Reg2ReadSelect(Read2Address),
	.WriteRegSelect(RegWriteAddress),
	.WriteEnable(RegWriteEnable),
	.WriteRegData(WriteRegData),
	.clock(clock),
	.reset(reset)
);

SRAM32 sram (
	.DATA(SRAMData),
	.ADDRESS(SRAMAddress * 11'd2),
	.CLK(clock),
	.WE(WriteSRAM),
	.OE(!OESRAM),
	.RST(reset)
);

always@(posedge clock) begin
	if(reset) begin
		NEW_CLOCK <= 0;
		CLK_VAL <= 0;
	end else begin
		NEW_CLOCK <= NEW_CLOCK + 25'b1;
	end
	
	if(speed_up) begin
		CLK_VAL <= 22;
	end
end

/*always@(posedge slowdown[22] or posedge reset) begin
	if (reset) begin
		slowcount <= 3'b0;
	end else begin
		slowcount <= slowcount + 1;
	end
end*/

always @(posedge NEW_CLOCK[CLK_VAL] or posedge reset) begin
	if (reset) begin
		populate_regfile <= 8'b0;
		populate_SRAM <= 8'b0;
		block <= 4'b0;
		write_block_to_SRAM <= 8'b0;
		slowdown <= 24'b0;
		write_SRAM <= 1'b0;
		read_SRAM <= 1'b0;
		WriteSRAMData <= 32'b0;
		SRAMAddress <= 11'b0;
		WriteSRAM <= 1'b0;
		OESRAM <= 1'b0;
		RegWriteEnable <= 1'b0;
		WriteRegData <= 32'b0;
		RegWriteAddress <= 5'b0;
		Read1Address <= 5'd31;
		Read2Address <= 5'd31;
		speed_up <= 1;
		slowcount <= 0;
	end
	else begin
		if (populate_SRAM < 256) begin
			
			WriteSRAM <= populate_SRAM[0];
			SRAMAddress <= {populate_SRAM[7:1]};
			if (populate_SRAM[0]) begin
				WriteSRAMData <= {25'b0, ~populate_SRAM[7:1]};
			end
			else begin
				WriteSRAMData <= 32'b0;
			end
			if (populate_SRAM == 255) begin
				read_SRAM <= 1'b1;
			end
			populate_SRAM <= populate_SRAM + 1;
		end
		else begin
			
			// prime data from regfile to write to SRAM and display
			if (write_SRAM && (slowcount == 2'd0)) begin
				Read1Address <= write_block_to_SRAM[4:0];
				Read2Address <= write_block_to_SRAM[4:0] + 5'd16;
			end
			// set up target address to read from SRAM
			if (read_SRAM && (slowcount == 2'd1)) begin
				OESRAM <= 1'b1;
				WriteSRAM <= 0;
				if (block == 2'd0) begin
					SRAMAddress <= {3'b0, populate_regfile};
				end else if (block == 2'd1) begin
					SRAMAddress <= {3'b0, populate_regfile + 8'd32};
				end else if (block == 2'd2) begin
					SRAMAddress <= {3'b0, populate_regfile + 8'd64};
				end else if (block == 2'd3) begin
					SRAMAddress <= {3'b0, populate_regfile + 8'd96};
				end
			end
			// set up data and target address to write to SRAM
			if (write_SRAM && (slowcount == 2'd1)) begin
				WriteSRAM <= 1'b1;
				if (block == 2'd0) begin
					SRAMAddress <= {3'b0, write_block_to_SRAM + 8'd128};
					WriteSRAMData <= Reg1ReadData;
				end
				else if (block == 2'd1) begin
					SRAMAddress <= {3'b0, write_block_to_SRAM + 8'd145};
					WriteSRAMData <= Reg2ReadData;
				end
				else if (block == 2'd2) begin
					SRAMAddress <= {3'b0, write_block_to_SRAM + 8'd162};
					WriteSRAMData <= Reg1ReadData;
				end
				else if (block == 2'd3) begin
					SRAMAddress <= {3'b0, write_block_to_SRAM + 8'd179};
					WriteSRAMData <= Reg2ReadData;
				end
			end
			if (slowcount == 2'd2) begin
				OESRAM <= 1'b0;
				WriteSRAM <= 1'b0;
				//iterate loop 32 times to populate regfile with SRAM data
				if (populate_regfile < 32) begin	
					RegWriteEnable <= 1'b1;
					RegWriteAddress <= populate_regfile;
					WriteRegData <= SRAMData;
					if (populate_regfile == 31) begin
						read_SRAM <= 1'b0;
						write_SRAM <= 1'b1;
						write_block_to_SRAM <= 8'b0;
						
					end
					populate_regfile <= populate_regfile + 1;
				end
				else if (write_block_to_SRAM < 16) begin
					if (write_block_to_SRAM == 15) begin
						if (block < 3) begin
							block <= block + 1;
							populate_regfile <= 8'b0;
							read_SRAM <= 1'b1;
							write_SRAM <= 1'b0;
						end else begin	// stop all processes once all 4 blocks are done
							read_SRAM <= 1'b0;
							write_SRAM <= 1'b0;
						end
					end
					write_block_to_SRAM <= write_block_to_SRAM + 1;
				end
			end
		end
		slowcount <= slowcount + 1;
	end
end

endmodule

module testbench_INTEG;
	
	wire clk;
	wire[1:0] KEY;
	wire[7:0] LEDR;
	
	SRAM_Reg_Integration uut(clk, KEY, LEDR);
	Test mytest(clk, KEY, LEDR);
	initial
		begin
			$dumpfile("INTEGRATION.vcd");
			$dumpvars(0, uut, mytest);
		end
endmodule

module Test (clk, KEY, LEDR);
	input[7:0] LEDR;
	output[1:0] KEY;
	output clk;
	
	reg clk;
	reg[1:0] KEY;
	
	parameter d = 20;
	
	initial clk = 0;
	always begin
		#(d/2)
		clk = ~clk;
	end
	
	initial
	begin
		KEY[1] = 1;
		KEY[0] = 0;
		#(d * 2) KEY[1] = 0;
		#(d * 2) KEY[1] = 1;
		#(d * 13000000) KEY[0] = 1;
		#(d * 16000000) KEY[0] = 0;
		$finish;
	end

endmodule