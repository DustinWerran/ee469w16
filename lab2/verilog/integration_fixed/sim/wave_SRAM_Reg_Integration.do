onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /testbench_INTEG/clk
add wave -noupdate /testbench_INTEG/uut/reset
add wave -noupdate {/testbench_INTEG/uut/KEY[0]}
add wave -noupdate /testbench_INTEG/LEDR
add wave -noupdate -expand -group Reg -radix hexadecimal /testbench_INTEG/uut/Reg1ReadData
add wave -noupdate -expand -group Reg -radix hexadecimal /testbench_INTEG/uut/Reg2ReadData
add wave -noupdate -expand -group Reg /testbench_INTEG/uut/RegWriteEnable
add wave -noupdate -expand -group Reg -radix hexadecimal /testbench_INTEG/uut/WriteRegData
add wave -noupdate -expand -group Reg /testbench_INTEG/uut/RegWriteAddress
add wave -noupdate -expand -group Reg /testbench_INTEG/uut/Read1Address
add wave -noupdate -expand -group Reg /testbench_INTEG/uut/Read2Address
add wave -noupdate -expand -group SRAM -radix hexadecimal /testbench_INTEG/uut/WriteSRAMData
add wave -noupdate -expand -group SRAM -radix hexadecimal /testbench_INTEG/uut/SRAMAddress
add wave -noupdate -expand -group SRAM /testbench_INTEG/uut/WriteSRAM
add wave -noupdate -expand -group SRAM /testbench_INTEG/uut/OESRAM
add wave -noupdate -expand -group SRAM -radix hexadecimal /testbench_INTEG/uut/SRAMData
add wave -noupdate /testbench_INTEG/uut/write_SRAM
add wave -noupdate /testbench_INTEG/uut/read_SRAM
add wave -noupdate /testbench_INTEG/uut/block
add wave -noupdate /testbench_INTEG/uut/populate_SRAM
add wave -noupdate /testbench_INTEG/uut/populate_regfile
add wave -noupdate /testbench_INTEG/uut/readout
add wave -noupdate /testbench_INTEG/uut/write_block_to_SRAM
add wave -noupdate /testbench_INTEG/uut/read_slow
add wave -noupdate /testbench_INTEG/uut/slowdown
add wave -noupdate /testbench_INTEG/uut/slowcount
add wave -noupdate /testbench_INTEG/uut/speed_up
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 326
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {810 ps}
