vlib work

vlog -work work ../SRAM_Reg_Integration.v
vlog -work work ../../SRAM32.v
vlog -work work ../Regfile.v
vlog -work work ../DFlipFlop.v
vlog -work work ../Decoder5to32.v
vlog -work work ../Decoder2to4.v
vlog -work work ../Bitcell.v
vlog -work work ../3to8Decoder.v

vsim -t 1ps -novopt testbench_INTEG

view signals
view wave

do wave_SRAM_Reg_Integration.do

run -all