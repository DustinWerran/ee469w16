module SRAM32(DATA, ADDRESS, CLK, WE, OE, RST);
	//----BI-DIRECTIONAL PORT----//
	inout [31:0] DATA;
	
	//---------INPUTS---------//
	
	input [10:0] ADDRESS;
	input CLK, WE, OE, RST;
	
	//---------REGISTERS-----------//
	reg [10:0] ADDRreg;
	reg [31:0] DATAreg, DATAOUT;
	reg load;
	//-----MEMORY REGISTER---------//
	reg [15:0] MEMORY [0:2023];
	//-------INTEGERS----------//
	integer i;
	
	assign DATA = !OE ? DATAOUT : 16'bz;
	
	always@(posedge CLK) begin
	
		//----RESET STATE-------//
		if(RST) begin
			for(i = 0; i < 2024; i = i + 1) begin
				MEMORY[i] <= 0;
			end
			ADDRreg <= 0;
			DATAreg <= 0;
			load <= 0;
		end else begin
			//-----WRITING----------//
			if(WE & OE) begin
				ADDRreg <= ADDRESS;
				DATAreg <= DATA;
				load <= 1;
			end
			
			if(load) begin
				{MEMORY[ADDRreg], MEMORY[ADDRreg + 1]} <= DATAreg;
				load <= 0;
			end
			//-------READING--------//
			if(!WE & !OE) begin
				DATAOUT <= {MEMORY[ADDRESS], MEMORY[ADDRESS + 1]}; 
			end
		end
	end
	
endmodule
/*
module testbench;

	wire CLK, RST, WE, OE;
	wire[10:0] ADDRESS;
	wire[31:0] DATA;

	SRAM32 	UTT(DATA, ADDRESS, CLK, WE, OE, RST);
	Test 	mytest(DATA, ADDRESS, CLK, WE, OE, RST);
	initial 
		begin
			$dumpfile("FAST_SRAM.vcd");
			$dumpvars(0, UTT, mytest);
		end
endmodule

module Test(DATA, ADDRESS, CLK, WE, OE, RST);
		//----BI-DIRECTIONAL PORT----//
	inout [31:0] DATA;
	
	//---------OUTPUTS---------//
	
	output [10:0] ADDRESS;
	output CLK, WE, OE, RST;
	
	//------REGISTERS--------//
	reg [31:0] DATA_VAL;
	reg [31:0] DATAOUT;
	reg [10:0] ADDRESS;
	reg CLK, WE, OE, RST;
	//-----------WIRE-----------//
	wire[31:0] DATAIN;
	//------ASSIGNMENTS------//
	assign DATAin = DATA;
	assign DATA = OE ? DATAOUT : 16'bz;
	//-----INTEGER-----------//
	integer i;
	//-----Simulated Clock------//
	always begin
		#1 CLK <= ~CLK;
	end
	
	initial begin
		$monitor(DATA_VAL);
		CLK = 0;
		WE = 0;
		OE = 1;
		ADDRESS = 0;
		DATA_VAL = 0;
		#10;
		for(i = 0; i < 128; i = i + 1) begin
			DATAOUT = DATA_VAL;
			WE = 1;
			#10;
			WE = 0;
			#10
			DATA_VAL = DATA_VAL + 1;
			ADDRESS = ADDRESS + 2;	
		end
		$display("---------READING FROM SRAM--------");
		ADDRESS = 0;
		for(i = 0; i < 128; i = i + 1) begin
			OE = 0;
			#5;
			DATA_VAL = DATA;
			#5;
			OE = 1;
			#10;
			ADDRESS = ADDRESS + 2;
		end
		
		
		$finish;
	end

endmodule
	
	*/