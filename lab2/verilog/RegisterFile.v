// Regfile On-Board Testing
module RegisterFileSRAM(
	output [7:0] LED,
	input [1:0] KEY,
	input CLOCK_50
);

reg [4:0] Reg1ReadSelect, Reg2ReadSelect;
reg [4:0] WriteRegSelect;
reg [31:0] WriteRegData;

reg [31:0] counter;

wire reset;
wire [31:0] Reg1ReadData, Reg2ReadData;
wire WriteEnable;

assign reset = KEY[1];
assign WriteEnable = !(&WriteRegSelect);

assign LED[7:0] = KEY[0] ? Reg2ReadData[7:0] : Reg1ReadData[7:0];

Regfile uut (
	.Reg1ReadData(Reg1ReadData), 
	.Reg2ReadData(Reg2ReadData),
	.Reg1ReadSelect(Reg1ReadSelect), 
	.Reg2ReadSelect(Reg2ReadSelect),
	.WriteRegSelect(WriteRegSelect),
	.WriteEnable(WriteEnable),
	.WriteRegData(WriteRegData),
	.clock(CLOCK_50),
	.reset(reset)
);

always @(posedge CLOCK_50) begin
	if (reset) begin
		Reg1ReadSelect <= 5'd0;
		Reg2ReadSelect <= 5'd16;
		WriteRegSelect <= 5'd0;
		WriteRegData <= 32'hFFFF000F;
		
		counter <= 32'd0;
	end
	else if (WriteRegSelect < 15) begin
		WriteRegData <= WriteRegData - 1;
	end
	else if (WriteRegSelect == 15) begin
		WriteRegData <= 32'h0000FFF0;
	end
	else if (WriteRegSelect < 31) begin
		WriteRegData <= WriteRegData + 1;
	end
	else begin
		if (counter == 32'd25000000) begin
			counter <= 32'd0;
			if (Reg1ReadSelect == 5'd15) begin
				Reg1ReadSelect <= 5'd0;
				Reg2ReadSelect <= 5'd16;
			end
			else begin
				Reg1ReadSelect <= Reg1ReadSelect + 1;
				Reg2ReadSelect <= Reg2ReadSelect + 1;
			end
		end
		else begin
			counter <= counter + 1;
		end
	end
end
	
endmodule
