module Bitcell(
	output r1, r2,
	input re1, re2,
	input w,
	input we,
	input clk,
	input rst
);

wire dff_out;

// dff with enable to hold bit
DFlipFlop datacell (.q(dff_out), .qBar(), .D(w), .en(we), .clk(clk), .rst(rst));

// tristates on two separate drive lines for output
bufif1 buf1(r1, dff_out, re1);
bufif1 buf2(r2, dff_out, re2);

endmodule
