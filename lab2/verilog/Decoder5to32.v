// the architecture for this module derived from
// an mit publication accessable at
// http://www.mitpublications.org/yellow_images/1393671424_logo_Paper_07.pdf
// it has been modified slightly for simplicity of application

module Decoder5to32(
	output [31:0] decoded,
	input [4:0] code
);

wire [7:0] predecode8;
wire [3:0] predecode4;

Decoder3to8 pre3to8 (predecode8, code[4:2]);
Decoder2to4 pre2to4 (predecode4, code[1:0]);

genvar i, j;

generate
	for (i = 0; i < 8; i = i + 1) begin : rows
		for (j = 0; j < 4; j = j + 1) begin : columns
			and(decoded[4*i + j], predecode8[i], predecode4[j]);
		end
	end
endgenerate

endmodule


module Decoder5to32_tb();

reg [4:0] code;
wire [31:0] decoded;

Decoder5to32 uut (decoded, code);

integer i;

initial begin
	code <= 5'b0;
	for (i = 0; i < 32; i = i + 1) begin
		code <= i;
		#50;
	end
	$stop;
end

endmodule
