module SRAM(DATA, ADDRESS, CLK, WE, OE, RST);
	inout [15:0] DATA;

	input [10:0] ADDRESS;
	input CLK, WE, OE, RST;
	//output [15:0] DATAout
	reg [15:0] Memory [0:2023];
	reg [15:0] DATAout;
	reg [31:0] DATAreg;
	reg [10:0] ADDRreg;
	reg high;
	reg load;
	reg write_nread;
	reg read_done, write_done;
	assign DATA = !OE ? DATAout : 16'bz;
	integer i;
	always@(posedge CLK)
		begin
			ADDRreg <= ADDRESS;
			if(RST) begin
				for(i = 0; i < 2023; i = i + 1) begin
					Memory[i] = 0;
				end
			end
			if(load) begin
					if(write_nread) begin
						{Memory[ADDRreg], Memory[ADDRreg + 1]} <= DATAreg;
						load = 0;		
					end else begin
						if(high) begin
							DATAout <= DATAreg[31:16];
							high = 0;
						
						end else begin
							DATAout <= DATAreg[15:0] ;
							high = 1;
							load = 0;
						end
						
					end
			end
			if(WE & write_done) begin
				if(high) begin
					DATAreg[15:0] <= DATA;
					high = 0;
				end else begin
					
					DATAreg[31:16] <= DATA;
					high = 1;
					load = 1;
					write_nread = 1;
					write_done = 0;
				end
				
			end else if(!WE) begin
				write_done = 1;
			end
			if(!OE & read_done & !load) begin
					DATAreg <= {Memory[ADDRreg], Memory[ADDRreg + 1]};
					load = 1;
					write_nread = 0;
					high = 1;
					read_done = 0;
			end	else if(OE) begin
				read_done = 1;
			end
		end


endmodule

module testbench;

	wire CLK, RST, WE, OE;
	wire[10:0] ADDRESS;
	wire[15:0] DATA;

	SRAM 	UTT(DATA, ADDRESS, CLK, WE, OE, RST);
	Test 	mytest(CLK, RST, DATA, ADDRESS, WE, OE);
	initial 
		begin
			$dumpfile("SRAM.vcd");
			$dumpvars(0, UTT, mytest);
		end
endmodule

module Test (CLK, RST, DATA, ADDRESS, WE, OE);
	inout [15:0] DATA;
	output CLK, RST, WE, OE;
	output [10:0] ADDRESS;
	
	reg [15:0] DATAout;
	wire [15:0] DATAin;
	reg CLK, RST, WE, OE;
	reg [10:0] ADDRESS;
	integer i = 0;
	reg[15:0] LOW;
	reg[15:0] HIGH;
	reg [31:0] value;
	reg write_done, read_done;
	reg high;
	assign DATAin = DATA;
	assign DATA = OE ? DATAout : 16'bz;
	
	
	
	
	
always begin
	#1 CLK <= ~CLK;
end

always @(posedge CLK) begin
	if(WE & write_done) begin
		if(high) begin
			DATAout <= value[15:0];
			high = 0;
		end else begin
			DATAout	 <= value[31:16];
			high = 1;
			write_done = 0;
		end
	end else if (!WE) begin 
		write_done = 1;
	end
	
	if(!OE & read_done) begin
		if(high) begin
			value[31:16] <= DATA;
			high = 0;
			@(posedge CLK);
		end else begin
			value[15:0] <= DATA;
			high = 1;
			read_done = 0;
		end
	end else if (OE) begin
		read_done = 1;
	end
end

always@(posedge high) begin
	//$display("DATA VAlue is %d", value);
end
integer x;
initial begin
	CLK = 0;
	value = 0;
	WE = 0;
	OE = 1;
	ADDRESS = 0;
	$monitor(DATA);
	for(x = 0; x < 128; x = x + 1) begin
			#10
			WE = 1;
			//$display("DATA is %d", DATA);
			#10
			//$display("DATA is %d", DATA);
			WE = 0;
			value = value + 1;
			ADDRESS = ADDRESS + 2;
	end
	ADDRESS = 0;
	value = 32'bz;
	$display("-------READING FROM SRAM------------");
	for(x = 0; x < 128; x = x + 1) begin
			#10
			OE = 0;
			#10
			OE = 1;
			
			ADDRESS = ADDRESS + 2;
	end
	$finish;
end 


endmodule