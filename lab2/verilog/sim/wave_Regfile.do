onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /Regfile_tb/clock
add wave -noupdate /Regfile_tb/reset
add wave -noupdate -radix hexadecimal /Regfile_tb/Reg1ReadData
add wave -noupdate -radix hexadecimal /Regfile_tb/Reg2ReadData
add wave -noupdate -radix unsigned /Regfile_tb/Reg1ReadSelect
add wave -noupdate -radix unsigned /Regfile_tb/Reg2ReadSelect
add wave -noupdate -radix hexadecimal /Regfile_tb/WriteRegData
add wave -noupdate -radix unsigned /Regfile_tb/WriteRegSelect
add wave -noupdate /Regfile_tb/WriteEnable
add wave -noupdate -radix hexadecimal /Regfile_tb/cross_register
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 182
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {957 ps}
