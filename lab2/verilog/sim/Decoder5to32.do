vlib work

vlog -work work ../Decoder5to32.v
vlog -work work ../Decoder2to4.v
vlog -work work ../3to8Decoder.v

vsim -t 1pS -novopt Decoder5to32_tb

view signals
view wave

do wave_Decoder5to32.do

run -all
