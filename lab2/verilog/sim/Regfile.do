vlib work

vlog -work work ../Decoder5to32.v
vlog -work work ../Decoder2to4.v
vlog -work work ../3to8Decoder.v

vlog -work work ../Bitcell.v
vlog -work work ../DFlipFlop.v

vlog -work work ../Regfile.v
vlog -work work ../Regfile_tb.v

vsim -t 1pS -novopt Regfile_tb

view signals
view wave

do wave_Regfile.do

run 10 us
