 `timescale 1ns/1ps

module Regfile_tb();

wire [31:0] Reg1ReadData, Reg2ReadData;
reg [4:0] Reg1ReadSelect, Reg2ReadSelect;
reg [4:0] WriteRegSelect;
reg WriteEnable;
reg [31:0] WriteRegData;
reg clock;
reg reset;

// store the values here as they're written to check against
// what is being stored in the regfile
reg [31:0] cross_register [31:0];

parameter half_clk50 = 10;

Regfile uut (
	.Reg1ReadData(Reg1ReadData), 
	.Reg2ReadData(Reg2ReadData),
	.Reg1ReadSelect(Reg1ReadSelect), 
	.Reg2ReadSelect(Reg2ReadSelect),
	.WriteRegSelect(WriteRegSelect),
	.WriteEnable(WriteEnable),
	.WriteRegData(WriteRegData),
	.clock(clock),
	.reset(reset)
);

initial begin
 	clock <= 1'b1;
	Reg1ReadSelect <= 5'b0;
	Reg2ReadSelect <= 5'b0;
	WriteRegSelect <= 5'b0;
	WriteEnable <= 1'b0;
	WriteRegData <= 32'b0;
	reset <= 1'b1;
	@(posedge clock);
	reset <= 1'b1;
	@(posedge clock);
	reset <= 1'b0;
	
end

always #(half_clk50) begin
	clock <= ~clock;
end

integer seed = 0;

always @(posedge clock) begin

	if (!reset) begin
		// check data in the simulation regfile
		if (cross_register[Reg1ReadSelect] != Reg1ReadData) begin
			$display("Reg 1 Read Error at %d", $time);
		end
		if (cross_register[Reg2ReadSelect] != Reg2ReadData) begin
			$display("Reg 2 Read Error at %d", $time);
		end
		if (WriteEnable) begin
			if (WriteRegSelect == 31) begin
				cross_register[WriteRegSelect] <= 32'b0;
			end
			else begin
				cross_register[WriteRegSelect] <= WriteRegData;
			end
		end

		// apply inputs to the regfile
		Reg1ReadSelect <= ($urandom(seed + 2)%32);
		Reg2ReadSelect <= ($urandom(seed + 3)%32);
		WriteRegSelect <= ($urandom(seed + 4)%32);
		WriteEnable <= ($urandom(seed + 5)%2);
		WriteRegData <= ($urandom(seed + 6));
		seed = seed + 10;
	end

end

endmodule