`include "SRAM32.v"

module SRAM_BOARD(CLOCK_50, LEDR[9:0], KEY[3:0]);
	input CLOCK_50;
	input [3:0] KEY;
	inout[31:0] DATA;
	//---OUTPUT-----//
	output [9:0] LEDR;
	//-----WIRES-------//
	
	//======registers======//
	wire[10:0] ADDRESS;
	reg[24:0] tbase;
	
	
	wire WE, OE;
	
	
	assign RST = KEY[0];
	assign LEDR = DATA[9:0];
	
	initial begin
		tbase <= 0;
	end
	
	always@(posedge CLOCK_50) begin
		if(RST) begin
			tbase <= 0;
		end else begin
			tbase = tbase + 1;
		end
	end
	
	
	SRAM32	mySRAM(.ADDRESS(ADDRESS), .CLK(tbase[22]), .WE(WE), .OE(OE), .RST(RST), .DATA(DATA));
	driver	mydriver(.ADDRESS(ADDRESS), .CLK(tbase[22]), .WE(WE), .OE(OE), .RST(RST), .DATA(DATA));

endmodule

module driver(DATA, ADDRESS, CLK, WE, OE, RST);
	
	//--------INPUTS-----------//
	input CLK, RST;
	
	//---------OUTPUTS---------//
	inout[31:0] DATA;
	output [10:0] ADDRESS;
	output WE, OE;
	
	//------REGISTERS--------//
	reg [31:0] DATA_VAL;
	reg [31:0] DATAOUT;
	reg WE, OE;
	reg[10:0] ADDRESS;
	//-----------WIRE-----------//
	wire[31:0] DATAIN;
	//------ASSIGNMENTS------//
	assign DATAin = DATA;
	assign DATA = OE ? DATAOUT : 16'bz;
	//-----INTEGER-----------//
	integer i;
	
	always@(posedge RST) begin
		
		WE = 0;
		OE = 1;
		ADDRESS = 0;
		DATA_VAL = 0;
		for(i = 0; i < 10; i = i + 1) begin
			@(posedge CLK);
		end
		for(i = 0; i < 128; i = i + 1) begin
			DATAOUT = DATA_VAL;
			WE = 1;
			for(i = 0; i < 10; i = i + 1) begin
				@(posedge CLK);
			end
			WE = 0;
			for(i = 0; i < 10; i = i + 1) begin
				@(posedge CLK);
			end
			DATA_VAL = DATA_VAL + 1;
			ADDRESS = ADDRESS + 2;	
		end
		$display("---------READING FROM SRAM--------");
		ADDRESS = 0;
		for(i = 0; i < 128; i = i + 1) begin
			OE = 0;
			for(i = 0; i < 5; i = i + 1) begin
				@(posedge CLK);
			end
			DATA_VAL = DATA;
			for(i = 0; i < 5; i = i + 1) begin
				@(posedge CLK);
			end
			OE = 1;
			for(i = 0; i < 10; i = i + 1) begin
				@(posedge CLK);
			end
			ADDRESS = ADDRESS + 2;
		end
	end
endmodule
	