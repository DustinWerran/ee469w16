#include <stdio.h>

#define HEADWIND (float) 89.6
#define AIRMILES (float) 4791.0

void estimate_duration(float time);
float calculate_estimated_duration(float velocity);
float get_velocity(float time);
float gathertime();

int main() {
	float time = 0.0;
	float velocity = 0.0;
	float adj_time = 0.0;

	time = gathertime();
	velocity = get_velocity(time);
	adj_time = calculate_estimated_duration(velocity);
	estimate_duration(adj_time);
	return 0;
}
/* Function gathers input from the user on how many hours it take to go from*/
/* Seattle to London*/
float gathertime() {
	float time;
	int valid = 1;
	while(valid) {
		printf("How long does it take to get from Seattle to London in hours?\n");
		scanf("%f", &time);
		getchar();
		if(time <= 0) {
			printf("Not a valid input! Try again\n");
		} else {
			valid = 0;
		}

	}
	return time;
}
//* Function takes the floating argument of time and devides the airmiles by time
//  to get the velocity of the plane
float get_velocity(float time) {
	float velocity = AIRMILES / time;
	printf("The velocity of an aircraft traveling from Seattle to London in %.2f hours\nis %.2f miles per hour.\n", time, velocity);
	return velocity;
}

//Function calculates the duration of the flight when accounting for headwind
float calculate_estimated_duration(float velocity) {
	float adj_velocity = AIRMILES / (velocity - HEADWIND);
	if(velocity <= 0) {
		printf("Your plane isn't moving at all");
		return 0;
	}
	return adj_velocity;
}
//Prints the duration of the flight when accounting for headwind
void estimate_duration(float time) {
	if(time == 0) {
		return;
	}
	printf("The estimated duration of the flight when accounting for a headwind of %.1f \nmiles is %.2f hours\n", HEADWIND, time);

}
