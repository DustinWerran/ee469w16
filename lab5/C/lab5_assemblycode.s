//Data Locations
//A = 0  	== r1
//B = 2		== r2
//C = 4		== r3
//D = 6		== r4
//D* = 8	== r5
//E = 10	== r6
//F = 12	== r7
//G = 14	== r8
//H = 16	== r9
//Constants Locations
//3 = 18
//4 = 20
//7 = 22
//8 = 24
//System Constants
//Repeat = 26
		//Constant register is r10-r11
		//Result register is r12
		//Loading Values for A and B into Reg File and constant 
		//Loading all values in program
main: 	SUB r0, r0, r0 //Setting r0 to 0
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000000000000000000000000

		LDURSW r1, r0, #0 //A being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000000000000000001
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r2, r0, #2 //B being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000010000000000010
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r3, r0, #4 //C being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000100000000000011
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r4, r0, #6 //D being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000110000000000100
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r5, r0, #8 //D* being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001000000000000101
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r6, r0, #10 //E being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001010000000000110
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r7, r0, #12 //F being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001100000000000111
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r8, r0, #14 //G being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001110000000001000
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r9, r0, #16 //H is being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010000000000001001

		LDURSW r10, r0, #18 //Constant 3 being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010010000000001010
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		SUB r12, r2, r1 //Subtracting A from B
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000000010000000001001100

		SUB r12, r10, r12 //Subtracting the result by the constatn 3
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000011000000000101001100

		B.GT r11, L1 #8//Comparing the result to see if greater than 0
		//OPCODE  :BT_ADDRESS    :RT   :
		01010100100000000000010000010011

		LDURSW r11, #22 //Loading constant value 7
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010111000000001011

		LSL r3, r3, #3  //Shiting the value of C by 3
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11010011011000000000110001100011

		STURW r3, r0, #4  //Stroing the value of C
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000100000000000011

		STURW r11, r5, #0 //Storing the value 7 into memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000000000100101011

		LDURSW r3, r0, #8 //Bring the new value of D back
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001000000000000011

		AND r8, r6, r7  //Andig F and G
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000001100000000011101000

		BR L2 #8:
		//OPCODe  :BR_ADDRESS          :
		11010110000000000000000000001000

L1:		LDURSW r11, r0, #20//Loading Value 4
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100101000000000000001011
      
		NOP  // wait for data to load
		11111111000000000000000000000000

		ADD r3, r3, r11 //Adding 4 to C
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000000110000000101100011

		STURW r3, r0, #4 //Storing Value into memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000100000000000011

		SUB r4, r3, r10 //Subtracting C by 3 and storing as D
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000000110000000101000100

		STURW r4, r0, #6 //Storing C
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000110000000000100

		ORR r8, r6, r7 //Logical Or of E and F storing in G
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10101010000001100000000100000111
		
		STURW r8, r0, #14  //Updating G in memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000001110000000001000

L2:		ADD r1, r1, r2 //Adding B to A 
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000000010000000001000001

		STURW r1, #0 //Storing A back into memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000000000000000001

		EOR r11, r6, r7 //Exclusize OR of E and F
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001010000001100000000011101011

		AND r8, r11, r9
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000010110000000100101000

		STURW r8, r0, #14
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000001110000000001000

		NOP
		11111111000000000000000000000000





		



