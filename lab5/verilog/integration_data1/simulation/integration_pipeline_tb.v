`timescale 1ns/1ps

module integration_pipeline_tb();

reg clk;
reg [3:0] KEY;

parameter half_clk = 2.5;

integration_single_cycle uut (
	.CLOCK_50(clk),
	.KEY(KEY)
);

always #half_clk
	clk = ~clk;
	
initial begin
	clk = 1'b1;
	KEY = 4'b1111;
end

initial begin
	@(posedge clk);
	KEY[0] = 1'b0;
	@(posedge clk);
	KEY[0] = 1'b1;
	forever
		@(posedge clk);
end

endmodule
