transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/pc_counter.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/left_shift_logic.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/kogge_stone.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/half_adder.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/grey_cell.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/DFlipFlop.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/Decoder5to32.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/Decoder2to4.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/Bitcell.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/add_sub.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/3to8Decoder.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/Regfile.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/mux2to1_32bit.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/integration_single_cycle.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/alu.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/black_cell.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/buffer32.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/flag_register.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/comparitor.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/buffer.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/buffer4.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/buffer3.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/mux3to1_32bit.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/buffer5.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/simulation {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/simulation/integration_pipeline_tb.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/SRAM32.v}
vlog -vlog01compat -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/instruction_memory.v}
vlog -sv -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/decode.sv}
vlog -sv -work work +incdir+C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source {C:/Users/eberq/Desktop/ee469w16/lab5/verilog/integration/source/forwarding_control.sv}

