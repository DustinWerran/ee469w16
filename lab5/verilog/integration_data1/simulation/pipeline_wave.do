onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /integration_pipeline_tb/clk
add wave -noupdate /integration_pipeline_tb/KEY
add wave -noupdate -expand -group Instruction -radix hexadecimal /integration_pipeline_tb/uut/im_read_address
add wave -noupdate -expand -group Instruction -radix binary /integration_pipeline_tb/uut/im_read_data
add wave -noupdate -expand -group Result -radix hexadecimal /integration_pipeline_tb/uut/data_out
add wave -noupdate -expand -group Result -radix hexadecimal /integration_pipeline_tb/uut/data_mux_ctl
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/z_flag
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/n_flag
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/c_flag
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/v_flag
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/flags
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/compare
add wave -noupdate -expand -group Flags -radix hexadecimal /integration_pipeline_tb/uut/update_flags
add wave -noupdate -expand -group Branching -radix hexadecimal /integration_pipeline_tb/uut/update_pc
add wave -noupdate -expand -group Branching -radix hexadecimal /integration_pipeline_tb/uut/update_pc_comp
add wave -noupdate -expand -group Branching -radix hexadecimal /integration_pipeline_tb/uut/buf4_branch_reg
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/ALU_mux0_ctrl
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/ALU_mux1_ctrl
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/bus_b
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/bus_a
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/alu_op0_from_id
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/alu_op1_from_id
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/sel_alu_op0
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/sel_alu_op1
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/alu_result
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/alu_a
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/alu_b
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/selected_alu_a
add wave -noupdate -expand -group ALU -radix hexadecimal /integration_pipeline_tb/uut/selected_alu_b
add wave -noupdate -expand -group RF -radix hexadecimal /integration_pipeline_tb/uut/rf_rd_addr_0
add wave -noupdate -expand -group RF -radix hexadecimal /integration_pipeline_tb/uut/rf_rd_addr_1
add wave -noupdate -expand -group RF -radix hexadecimal /integration_pipeline_tb/uut/rf_wr_addr
add wave -noupdate -expand -group RF -radix hexadecimal /integration_pipeline_tb/uut/rf_wr_en
add wave -noupdate -expand -group SRAM -radix hexadecimal /integration_pipeline_tb/uut/nWE
add wave -noupdate -expand -group SRAM -radix hexadecimal /integration_pipeline_tb/uut/OE
add wave -noupdate -expand -group SRAM -radix hexadecimal /integration_pipeline_tb/uut/sram_out
add wave -noupdate -expand -group SRAM /integration_pipeline_tb/uut/dm/ADDRESS
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {22819 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {64 ns}
