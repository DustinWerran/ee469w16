vlib work

vlog -work work ../source/SRAM32.v

vlog -work work ../source/Decoder5to32.v
vlog -work work ../source/Decoder2to4.v
vlog -work work ../source/3to8Decoder.v
vlog -work work ../source/Bitcell.v
vlog -work work ../source/DFlipFlop.v
vlog -work work ../source/Regfile.v

vlog -work work ../source/add_sub.v
vlog -work work ../source/kogge_stone.v
vlog -work work ../source/half_adder.v
vlog -work work ../source/black_cell.v
vlog -work work ../source/grey_cell.v
vlog -work work ../source/left_shift_logic.v
vlog -work work ../source/alu.v

vlog -work work ../source/instruction_memory.v
vlog -work work ../source/decode.sv
vlog -work work ../source/pc_counter.v
vlog -work work ../source/flag_register.v
vlog -work work ../source/comparitor.v
vlog -work work ../source/forwarding_control.sv

vlog -work work ../source/mux2to1_32bit.v
vlog -work work ../source/mux3to1_32bit.v
vlog -work work ../source/buffer32.v
vlog -work work ../source/buffer.v
vlog -work work ../source/buffer4.v
vlog -work work ../source/buffer5.v
vlog -work work ../source/buffer3.v

vlog -work work ../source/integration_single_cycle.v

vlog -work work ./integration_pipeline_tb.v

vsim -t 1ps -novopt integration_pipeline_tb

view signals
view wave

do wave_integration_pipeline.do

run 350 nS