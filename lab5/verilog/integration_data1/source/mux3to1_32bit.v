module mux3to1_32bit (in_0, in_1, in_2, Out, Sel);
	input [31:0] in_0, in_1, in_2;
	input [1:0] Sel;
	output reg [31:0] Out;
	
	always @(*) begin
		case (Sel)
			2'b00: Out = in_0;
			2'b10: Out = in_1;
			2'b11: Out = in_2;
			default: Out = 32'd0;
		endcase
	end
	
endmodule
	