module Decoder2to4(
	output [3:0] decoded,
	input [1:0] code
);

wire [1:0] code_n;

not(code_n[0], code[0]);
not(code_n[1], code[1]);

and(decoded[0], code_n[1], code_n[0]);
and(decoded[1], code_n[1], code[0]);
and(decoded[2], code[1], code_n[0]);
and(decoded[3], code[1], code[0]);

endmodule
