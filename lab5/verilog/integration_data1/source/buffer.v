module buffer (
	input in,
	input clock, reset,
	output reg out
);

always @(posedge clock or posedge reset)
begin
	if (reset) begin
		out <= 1'b0;
	end else begin
		out <= in;
	end
end

endmodule