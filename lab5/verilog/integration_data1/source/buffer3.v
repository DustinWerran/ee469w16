module buffer3 (
	input [2:0] in,
	input clock, reset,
	output reg [2:0] out
);

always @(posedge clock or posedge reset)
begin
	if (reset) begin
		out <= 3'd0;
	end else begin
		out <= in;
	end
end

endmodule