`timescale 1ns/10ps

// though it is hard to know if this will synthesize into a 
// barrel or a funnel shifter without actually writing it structurally,
// I wrote the logic in a way that at least implies a barrel shifter,
// and I think well-reproduces the design example given in class

module left_shift_logic (
	input [15:0] A,
	input [1:0] shift_amt,
	output [15:0] result
);


reg [15:0] layer1, layer2;

assign result = layer2;

integer i;

always @(*) begin
	for (i = 0; i < 16; i = i + 1) begin
		layer1 = shift_amt[1] ? {A[13:0], 2'b0} : A;
		layer2 = shift_amt[0] ? {layer1[14:0], 1'b0} : layer1;
	end
end

endmodule


module left_shift_logic_tb();

reg [15:0] A;
reg [1:0] shift_amt;
wire [15:0] result;

left_shift_logic uut (.A(A), .shift_amt(shift_amt), .result(result));

parameter delay = 1;

always #(delay) begin
	if (result != A << shift_amt) begin
		$display($time, " bad shift");
	end
	
	A <= $urandom;
	shift_amt <= $urandom;
end

endmodule
