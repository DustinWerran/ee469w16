// classic kogge_stone adder
// carry in bit is treated as bit 0, instead of -1
// extraneous wires were left undriven as opposed to
// 'b0 as I think leaving the wires undriven helps 
// identify them in testing, and either way they're
// going to be optimized away

// testing kogge_stone_tb.v

module kogge_stone (
	input [15:0] A, B,
	input Ci,
	output reg [15:0] S,
	output V, C
);

wire [16:0] P0, G0;
wire [15:0] P1, G1; 
wire [15:0] P2, G2;
wire [15:0] P3, G3;

// final carries, fed to xors
wire [16:0] carries;  

// instanciate the half adders at the top
half_adder ha (.A(A), .B(B), .G(G0[16:1]), .P(P0[16:1]));

// generate[0] is just carry in
assign G0[0] = Ci;

// drop down all the wires
assign G1[0] = G0[0];
assign G2[1:0] = G1[1:0];
assign G3[3:0] = G2[3:0];

// first seven carries drop down
assign carries[7:0] = G3[7:0]; 

// first layer
grey_cell #(1) gc1 (
	.Gitok(G0[1]),
	.Pitok(P0[1]),
	.Gkminusonetoj(G0[0]),
	.Gitoj(G1[1])
);
black_cell #(14) bc15to1 (
	.Gitok(G0[15:2]), 
	.Pitok(P0[15:2]), 
	.Gkminusonetoj(G0[14:1]), 
	.Pkminusonetoj(P0[14:1]),
	.Gitoj(G1[15:2]),
	.Pitoj(P1[15:2])
);

// second layer
grey_cell #(2) gc3to2 (
	.Gitok(G1[3:2]),
	.Pitok(P1[3:2]),
	.Gkminusonetoj(G1[1:0]),
	.Gitoj(G2[3:2])
);
black_cell #(12) bc15to4 (
	.Gitok(G1[15:4]), 
	.Pitok(P1[15:4]), 
	.Gkminusonetoj(G1[13:2]), 
	.Pkminusonetoj(P1[13:2]),
	.Gitoj(G2[15:4]),
	.Pitoj(P2[15:4])
);

// third layer
grey_cell #(4) gc7to4 (
	.Gitok(G2[7:4]),
	.Pitok(P2[7:4]),
	.Gkminusonetoj(G2[3:0]),
	.Gitoj(G3[7:4])
);
black_cell #(8) bc15to8 (
	.Gitok(G2[15:8]), 
	.Pitok(P2[15:8]), 
	.Gkminusonetoj(G2[11:4]), 
	.Pkminusonetoj(P2[11:4]),
	.Gitoj(G3[15:8]),
	.Pitoj(P3[15:8])
);

// fourth layer
grey_cell #(8) gc15to8 (
	.Gitok(G3[15:8]),
	.Pitok(P3[15:8]),
	.Gkminusonetoj(G3[7:0]),
	.Gitoj(carries[15:8])
);

// carry out cell
grey_cell #(1) gc16 (
	.Gitok(G0[16]),
	.Pitok(P0[16]),
	.Gkminusonetoj(carries[15]),
	.Gitoj(carries[16])
);

// xor block
integer i;

always @(*) begin
	for (i = 0; i < 16; i = i + 1) begin
		S[i] = carries[i] ^ P0[i+1];
	end
end

// flag assignment
assign V = carries[15] ^ carries[16];
assign C = carries[16];

endmodule



