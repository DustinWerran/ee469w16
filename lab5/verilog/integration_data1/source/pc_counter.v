module pc_counter(
	pc_count,
	value_in,
	branch,
	clk,
	rst
);

output [31:0] pc_count;
input [31:0] value_in;
input branch, clk, rst;
reg [31:0] pc_count_reg;
reg [31:0] next_count;
 
assign pc_count = pc_count_reg;

always @(posedge clk or posedge rst) begin
	if(rst) begin
		pc_count_reg <= 0;
	end else begin
		pc_count_reg <= next_count;
	end
end

always @(*) begin
	if (branch) begin
		next_count = value_in;
	end
	else begin
		next_count = pc_count_reg + 32'b1;
	end
end

endmodule