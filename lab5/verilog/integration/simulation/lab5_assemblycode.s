//Data Locations
//A = 2  	== r1  7
//B = 4		== r2  5
//C = 6		== r3  3
//D = 8		== r4  5
//D* = 10	== r5  6
//E = 12	== r6  5a5a
//F = 14	== r7  6767
//G = 16	== r8  3c
//H = 18	== r9  ff
//Constants Locations
//3 = 20
//4 = 22
//7 = 24
//8 = 26
//System Constants
//Repeat = 26
		//Constant register is r10-r11
		//Result register is r12
		//Loading Values for A and B into Reg File and constant 
		//Loading all values in program
main: 	NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		LDURSW r1, r0, #2 //A being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000000000000000000
		


		LDURSW r1, r0, #2 //A being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000010000000000001
		
		
		
		
		

		LDURSW r2, r0, #4 //B being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000100000000000010
		
		

		LDURSW r3, r0, #6 //C being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000000110000000000011
		
		

		LDURSW r4, r0, #8 //D being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001000000000000100
		
		

		LDURSW r5, r0, #10 //D* being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001010000000000101
		
		

		LDURSW r6, r0, #12 //E being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001100000000000110
		
		

		LDURSW r7, r0, #14 //F being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001110000000000111
		
		

		LDURSW r8, r0, #16 //G being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010000000000001000
		
		

		LDURSW r9, r0, #18 //H is being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010010000000001001
		
		LDURSW r10, r0, #20 //Constant 3 being loaded
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010100000000001010
		
		
		
		

		SUB r12, r2, r1 //Subtracting A from B
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000000100000000000101100
		

		SUB r12, r10, r12 //Subtracting the result by the constatn 3
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000010100000000110001100
		
		
		

		B.GT r11, L1 #8//Comparing the result to see if greater than 0
		//OPCODE  :BT_ADDRESS    :RT   :
		01010100100000000000001000010011
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000

		LDURSW r11, #24 //Loading constant value 7
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000011000000000001011

		LSL r3, r3, #3  //Shiting the value of C by 3
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11010011011000000000110001100011

		STURW r3, r0, #6  //Stroing the value of C
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000110000000000011

		STURW r11, r5, #2 //Storing the value 7 into memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000010000100101011

		LDURSW r3, r0, #10 //Bring the new value of D back
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000001010000000000011

		AND r8, r6, r7  //Andig F and G
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000001100000000011101000

		B L2 #8:
		//OPCODe  :BR_ADDRESS          :
		00010100000000000000000000001100
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000
		
		NOP  // wait for data to load
		11111111000000000000000000000000

L1:		LDURSW r11, r0, #22//Loading Value 4
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000100000010110000000001011
      
		

		ADD r3, r3, r11 //Adding 4 to C
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000000110000000101100011

		STURW r3, r0, #6 //Storing Value into memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000110000000000011

		SUB r4, r3, r10 //Subtracting C by 3 and storing as D
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001011000000110000000101000100

		STURW r4, r0, #8 //Storing C
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000001000000000000100

		ORR r8, r6, r7 //Logical Or of E and F storing in G
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10101010000001100000000100000111
		
		STURW r8, r0, #16  //Updating G in memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000010000000000001000

L2:		ADD r1, r1, r2 //Adding B to A 
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000000010000000001000001

		STURW r1, #2 //Storing A back into memory
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000000010000000000001

		EOR r11, r6, r7 //Exclusize OR of E and F
		//OPCODE  :RM  :SHM  :RN  :RD  :
		11001010000001100000000011101011

		AND r8, r11, r9
		//OPCODE  :RM  :SHM  :RN  :RD  :
		10001011000010110000000100101000

		STURW r8, r0, #16
		//OPCODE  :DT-     :X:RN  :RT  :
		10111000000000010000000000001000

		NOP
		11111111000000000000000000000000





		



