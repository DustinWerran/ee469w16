module Decoder3to8(
	output [7:0] decoded,
	input [2:0] code
);

wire [2:0] code_n;

genvar i;

generate
	for (i = 2; i >= 0; i = i - 1) begin : Inverters
		not inv (code_n[i], code[i]);
	end
endgenerate

and(decoded[0], code_n[2], code_n[1], 	code_n[0]);
and(decoded[1], code_n[2], code_n[1], 	code[0]	);
and(decoded[2], code_n[2], code[1], 	code_n[0]);
and(decoded[3], code_n[2], code[1], 	code[0]	);
and(decoded[4], code[2], 	code_n[1], 	code_n[0]);
and(decoded[5], code[2], 	code_n[1], 	code[0]	);
and(decoded[6], code[2], 	code[1], 	code_n[0]);
and(decoded[7], code[2], 	code[1], 	code[0]);

endmodule


module Decoder3to8_tb();

reg [2:0] code;
wire [7:0] decoded;

Decoder3to8 uut(.decoded(decoded), .code(code));

integer i;

initial begin
	for (i = 0; i < 8; i = i + 1) begin
		code <= i;
		#50;
	end
	$stop;
end

endmodule
