module buffer4 (
	input [3:0] in,
	input clock, reset,
	output reg [3:0] out
);

always @(posedge clock or posedge reset)
begin
	if (reset) begin
		out <= 4'd0;
	end else begin
		out <= in;
	end
end

endmodule