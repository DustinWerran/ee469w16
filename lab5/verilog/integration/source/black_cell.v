module black_cell #(
	parameter width = 16
) (
	input [width-1:0] Gitok,
	input [width-1:0] Pitok,
	input [width-1:0] Gkminusonetoj,
	input [width-1:0] Pkminusonetoj,
	output reg [width-1:0] Gitoj,
	output reg [width-1:0] Pitoj
);

integer i;

always @(*) begin
	for (i = 0; i < width; i = i + 1) begin
		Gitoj[i] = Gitok[i] | (Pitok[i] & Gkminusonetoj[i]);
		Pitoj[i] = Pitok[i] & Pkminusonetoj[i];
	end
end

endmodule
