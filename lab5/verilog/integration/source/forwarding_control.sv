// Controls data forwarding in the architecture. The data itself
// is not forwarded through this module, but instead this module
// checks for possible hazards and controls muxes that do the
// actual forwarding. Since the final register in the register
// file (reg 31) is always zero (it cannot be written to), it is
// safe to default the values to that register, as it will never
// cause a forward.

module forwarding_control (
	input clk,
	input rst,
	
	input [4:0] current_destination_reg,  
	// reg being written in most recently edited instruction
	input [4:0] current_source_reg_a,
	input [4:0] current_source_reg_b,
	// regs being read from in most recently edited instruction
	
	output reg [1:0] ALU_mux0_ctrl,
	output reg [1:0] ALU_mux1_ctrl
);

reg [4:0] EX_destination_reg, MEM_destination_reg;

always_ff @(posedge clk or posedge rst) begin
	if (rst) begin
		EX_destination_reg <= 5'd31;
		MEM_destination_reg <= 5'd31;
	end
	else begin
		EX_destination_reg <= current_destination_reg;
		MEM_destination_reg <= EX_destination_reg;
	end
end

// Making this operation combinational increases the
// critical delay for the execute stage. If the critical path
// ends up higher than the minimum clock period, this would
// be the first thing to change.
always_comb begin 
	ALU_mux0_ctrl = 2'b00;
	ALU_mux1_ctrl = 2'b00;

	if (EX_destination_reg != 5'd31) begin
		if (current_source_reg_a == EX_destination_reg) begin
			ALU_mux0_ctrl = 2'b10;
		end
		if (current_source_reg_b == EX_destination_reg) begin
			ALU_mux1_ctrl = 2'b10;
		end
	end
	if (MEM_destination_reg != 5'd31) begin
		if (current_source_reg_a == MEM_destination_reg) begin
			ALU_mux0_ctrl = 2'b11;
		end
		if (current_source_reg_b == MEM_destination_reg) begin
			ALU_mux1_ctrl = 2'b11;
		end
	end
end

endmodule
