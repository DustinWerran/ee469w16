module buffer5 (
	input [4:0] in,
	input clock, reset,
	output reg [4:0] out
);

always @(posedge clock or posedge reset)
begin
	if (reset) begin
		out <= 5'd0;
	end else begin
		out <= in;
	end
end

endmodule