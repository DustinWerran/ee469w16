// Does flag comparison. The only comparison implemented
// for this project is GT, so it can be assumed that if
// a comparison is needed, it's going to be GT.

module comparitor (
	input compare,
	input [3:0] FLAGS,
	output reg update_pc
);

always @(*)
	if (compare && FLAGS[3] == 1'b0 && FLAGS[2] == FLAGS[0])
		update_pc = 1'b1;
	else
		update_pc = 1'b0;
		
endmodule
