module decode (

	input clk,
	input rst,
	
	input [31:0] im_read_data,  // data from im
	input [31:0] pc_count,
	
	output reg [2:0] ALU_ctl,  // function select
	
	output reg data_mux_ctl,  // D sel
	
	output reg [31:0] alu_op0_from_id,  // constant
	output reg [31:0] alu_op1_from_id,  // constant
	
	output reg sel_alu_op0,  // controls ALU input mux
	output reg sel_alu_op1,  // controls ALU input mux
	
	output reg [4:0] rf_rd_addr_0,  // A adx
	output reg [4:0] rf_rd_addr_1,  // B adx
	output reg [4:0] rf_wr_addr,  // Reg Sel
	output reg rf_wr_en,  // RW
	
	output reg nWE,  // data mem write enable
	output reg OE,  // data mem output enable
	
	output reg update_pc,
	output reg update_flags,
	output reg compare
	
);


localparam NOP    = 32'b11111111000000000000000000000000,
			  ADD    = 32'b10001011000?????????????????????,
			  SUB    = 32'b11001011000?????????????????????,
			  AND    = 32'b10001010000?????????????????????,
			  ORR    = 32'b10101010000?????????????????????,
			  EOR    = 32'b11001010000?????????????????????,
			  LSL    = 32'b11010011011?????????????????????,
			  LDURSW = 32'b10111000100?????????????????????,
			  STURW  = 32'b10111000000?????????????????????,
			  B      = 32'b000101??????????????????????????,
			  BR     = 32'b11010110000?????????????????????,
			  B_cond = 32'b01010100????????????????????????;

			  
always_comb begin
	
	ALU_ctl = 3'b0;  // nop
	data_mux_ctl = 1'bx;
	alu_op0_from_id = 16'bx;
	alu_op1_from_id = 16'bx;
	sel_alu_op0 = 1'b0;
	sel_alu_op1 = 1'b0;
	rf_rd_addr_0 = 5'd31;
	rf_rd_addr_1 = 5'd31;
	rf_wr_addr = 5'd31;
	rf_wr_en = 1'b0;
	nWE = 1'b1;  // low
	OE = 1'b0;
	
	update_pc = 1'b0;
	update_flags = 1'b0;
	compare = 1'b0;

	
	unique casez (im_read_data)
		
		ADD: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
		
		end
		
		SUB: begin
		
			ALU_ctl = 3'b010;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[20:16];
			rf_rd_addr_1 = im_read_data[9:5];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
			
		end
		
		AND: begin
		
			ALU_ctl = 3'b011;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
			
		end
		
		ORR: begin
		
			ALU_ctl = 3'b100;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
			
		end
		
		EOR: begin
		
			ALU_ctl = 3'b101;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;

		end
		
		LSL: begin
		
			ALU_ctl = 3'b110;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			alu_op1_from_id = im_read_data[15:10];
			sel_alu_op1 = 1'b1;
			
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
			
		end
		
		LDURSW: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctl = 1'b1;
			OE = 1'b1;
			
			rf_rd_addr_0 = im_read_data[9:5];
			alu_op1_from_id = im_read_data[20:12];
			sel_alu_op1 = 1'b1;
			
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
			
		end
		
		STURW: begin
		
			ALU_ctl = 3'b001;
			
			nWE = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			alu_op1_from_id = im_read_data[20:12];
			sel_alu_op1 = 1'b1;
			
			rf_rd_addr_1 = im_read_data[4:0];
		
		end
		
		B: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctl = 1'b0;
			
			alu_op0_from_id = im_read_data[25:0];
			alu_op1_from_id = pc_count;
			sel_alu_op0 = 1'b1;
			sel_alu_op1 = 1'b1;
			
			update_pc = 1'b1;
		
		end
		
		BR: begin
		
			ALU_ctl = 3'b110;
			
			data_mux_ctl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[4:0];
			alu_op1_from_id = 32'b0;  // shift zero
			sel_alu_op1 = 1'b1;
		
			update_pc = 1'b1;
		
		end
		
		B_cond: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctl = 1'b0;
			
			alu_op0_from_id = im_read_data[25:5];
			alu_op1_from_id = pc_count;
			sel_alu_op0 = 1'b1;
			sel_alu_op1 = 1'b1;
		
			update_flags = 1'b1;
			compare = 1'b1;
		
		end
		
		// NOP: do nothing
		
	endcase
end

endmodule
