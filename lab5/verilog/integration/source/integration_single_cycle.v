module integration_single_cycle(
	input CLOCK_50, 
	input [3:0] KEY
);
	
	wire [31:0] im_read_address, buf_im_read_address;
	wire [31:0] im_read_data, buf_im_read_data;
	
	wire [31:0] data_out, buf_data_out;
	wire data_mux_ctl, buf_data_mux_ctl, buf2_data_mux_ctl, buf3_data_mux_ctl;
	
	wire z_flag, n_flag, c_flag, v_flag;
	wire buf_z_flag, buf_n_flag, buf_c_flag, buf_v_flag;
	wire update_pc;
	wire update_pc_comp, buf_update_pc_comp;
	wire compare, buf_compare, buf2_compare, buf3_compare;
	wire branch_reg, buf_branch_reg, buf2_branch_reg, buf3_branch_reg, buf4_branch_reg;
	
	wire update_flags, buf_update_flags, buf2_update_flags;
	wire [3:0] flags, buf_flags;
	
	wire [2:0] ALU_ctl, buf_ALU_ctl;
	wire [1:0] ALU_mux0_ctrl, ALU_mux1_ctrl;
	wire [31:0] bus_b, bus_a;
	wire [31:0] alu_op0_from_id, alu_op1_from_id;
	wire [31:0] alu_op0_id_extend, alu_op1_id_extend;
	wire sel_alu_op0, sel_alu_op1;
	
	wire [31:0] alu_result;
	wire [31:0] buf_alu_result, buf2_alu_result;
	wire [31:0] alu_a, alu_b;
	wire [31:0] buf_alu_a, buf_alu_b;
	wire [31:0] selected_alu_a, selected_alu_b;
	
	wire [4:0] rf_rd_addr_0, rf_rd_addr_1, rf_wr_addr;
	wire [4:0] buf_rf_rd_addr_0, buf_rf_rd_addr_1;
	wire [4:0] buf_rf_wr_addr, buf2_rf_wr_addr, buf3_rf_wr_addr, buf4_rf_wr_addr;
	wire rf_wr_en, buf_rf_wr_en, buf2_rf_wr_en, buf3_rf_wr_en, buf4_rf_wr_en;
	
	wire nWE, OE, buf_nWE, buf_OE, buf2_OE, buf3_OE, buf4_OE, buf2_nWE;
	wire [31:0] sram_out;
	wire [31:0] buf_sram_out;
	
	
	assign sram_out = buf2_OE ? 32'bZ : buf_alu_b;
	assign branch_reg = update_pc;
	
	
	decode decoder(
		// inputs
		.clk(CLOCK_50),
		.rst(!KEY[0]),
		.im_read_data(buf_im_read_data),
		.pc_count(buf_im_read_address),
		// outputs
		.ALU_ctl(ALU_ctl),
		.data_mux_ctl(data_mux_ctl),
		.alu_op0_from_id(alu_op0_from_id),
		.alu_op1_from_id(alu_op1_from_id),
		.sel_alu_op0(sel_alu_op0),
		.sel_alu_op1(sel_alu_op1),
		.rf_rd_addr_0(rf_rd_addr_0),
		.rf_rd_addr_1(rf_rd_addr_1),
		.rf_wr_addr(rf_wr_addr),
		.rf_wr_en(rf_wr_en),
		.nWE(nWE),
		.OE(OE),
		.update_pc(update_pc),
		.update_flags(update_flags),
		.compare(compare)
	);
	
	pc_counter pc(
		.pc_count(im_read_address),
		.value_in(buf_data_out),
		.branch(buf4_branch_reg | buf_update_pc_comp),
		.clk(CLOCK_50),
		.rst(!KEY[0])
	);
	
	instruction_memory im(
		.instruction(im_read_data),
		.address(im_read_address[6:0])
	);
	
	Regfile rf(
		.Reg1ReadData(bus_a), //rf outputA goes to ALU
		.Reg2ReadData(bus_b), //rf outputB
		.Reg1ReadSelect(rf_rd_addr_0),
		.Reg2ReadSelect(rf_rd_addr_1),
		.WriteRegSelect(buf4_rf_wr_addr),
		.WriteEnable(buf4_rf_wr_en),
		.WriteRegData(buf_data_out), //output from D sel mux
		.clock(CLOCK_50),
		.reset(!KEY[0])
	);
	
	alu myalu(
		.A(selected_alu_a), //input to ALU comes from regfile 1 read data, see sel_alu_op0
		.B(selected_alu_b), //input to ALU comes from regfile 2 read data, see sel_alu_op1
		.ctl(buf_ALU_ctl),
		.result(alu_result), //output from ALU, goes to Dsel and data memory
		.Z(z_flag),
		.N(n_flag),
		.C(c_flag),
		.V(v_flag)
	);
	
	SRAM32 dm(
		.DATA(sram_out),
		.ADDRESS(buf_alu_result[10:0]), 
		.CLK(CLOCK_50),
		.nWE(buf2_nWE),
		.OE(buf2_OE)
	);
	
	flag_register fr(
		.update_flags(buf_update_flags),
		.z_flag(buf_z_flag),
		.n_flag(buf_n_flag),
		.c_flag(buf_c_flag),
		.v_flag(buf_v_flag),
		.FLAGS(flags)
	);
	
	comparitor mycomp(
		.compare(buf3_compare),
		.FLAGS(buf_flags),
		.update_pc(update_pc_comp)
	);
	
	forwarding_control fc(
		.clk(CLOCK_50),
		.rst(!KEY[0]),
		.current_destination_reg(buf_rf_wr_addr),  
		.current_source_reg_a(buf_rf_rd_addr_0),
		.current_source_reg_b(buf_rf_rd_addr_1),
		.ALU_mux0_ctrl(ALU_mux0_ctrl),
		.ALU_mux1_ctrl(ALU_mux1_ctrl)
	);
	
	// muxes for datapath control
	
	mux2to1_32bit Dsel(
		.in_0(buf2_alu_result),
		.in_1(buf_sram_out),
		.Out(data_out),
		.Sel(buf3_data_mux_ctl)
	);
	
	mux2to1_32bit Asel(
		.in_0(bus_a),
		.in_1(alu_op0_from_id),
		.Out(alu_a),
		.Sel(sel_alu_op0)
	);
	
	mux2to1_32bit Bsel(
		.in_0(bus_b),
		.in_1(alu_op1_from_id),
		.Out(alu_b),
		.Sel(sel_alu_op1)
	);
	
	mux3to1_32bit alu_in_0(
		.in_0(buf_alu_a),
		.in_1(buf_alu_result),
		.in_2(buf_sram_out),
		.Out(selected_alu_a),
		.Sel(ALU_mux0_ctrl)
	);
	
	mux3to1_32bit alu_in_1(
		.in_0(buf_alu_b),
		.in_1(buf_alu_result),
		.in_2(buf_sram_out),
		.Out(selected_alu_b),
		.Sel(ALU_mux1_ctrl)
	);
	
	/* ---- buffers for pipelining ---- */
	
	buffer32 instruction_buf(
		.in(im_read_data),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_im_read_data)
	);
	
	buffer32 address_buf(
		.in(im_read_address),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_im_read_address)
	);
	
	// flag buffering
	
	buffer c_f(
		.in(c_flag),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_c_flag)
	);
	
	buffer n_f(
		.in(n_flag),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_n_flag)
	);
	
	buffer v_f(
		.in(v_flag),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_v_flag)
	);
	
	buffer z_f(
		.in(z_flag),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_z_flag)
	);
	
	buffer4 flagerinos(
		.in(flags),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_flags)
	);
	
	buffer udf1(
		.in(update_flags),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_update_flags)
	);
	
	// alu buffering
	
	buffer3 alu_ctl(
		.in(ALU_ctl),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_ALU_ctl)
	);
	
	buffer32 ALU_a_buf(
		.in(alu_a),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_alu_a)
	);
	
	buffer32 ALU_b_buf(
		.in(alu_b),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_alu_b)
	);
	
	buffer32 alu_out_buf(
		.in(alu_result),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_alu_result)
	);
	
	buffer32 alu_out_buf2(
		.in(buf_alu_result),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_alu_result)
	);
	
	buffer5 reg_0_read(
		.in(rf_rd_addr_0),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_rf_rd_addr_0)
	);
	
	buffer5 reg_1_read(
		.in(rf_rd_addr_1),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_rf_rd_addr_1)
	);
	
	buffer5 reg_write(
		.in(rf_wr_addr),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_rf_wr_addr)
	);
	
	// data memory buffering

	buffer32 datamem_buf(
		.in(sram_out),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_sram_out)
	);
	
	buffer OE_buf(
		.in(OE),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_OE)
	);
	
	buffer OE2(
		.in(buf_OE),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_OE)
	);
	/*
	buffer OE3(
		.in(buf2_OE),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf3_OE)
	);
	
	buffer OE4(
		.in(buf3_OE),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf4_OE)
	);
	*/
	buffer nWE_buf(
		.in(nWE),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_nWE)
	);
	
	buffer nWE2(
		.in(buf_nWE),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_nWE)
	);
	
	// comparitor buffer
	
	buffer compare1(
		.in(compare),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_compare)
	);
	
	buffer compare2(
		.in(buf_compare),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_compare)
	);
	
	buffer compare3(
		.in(buf2_compare),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf3_compare)
	);
	
	// regfile buffers
	
	buffer rfwe1(
		.in(rf_wr_en),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_rf_wr_en)
	);
	
	buffer rfwe2(
		.in(buf_rf_wr_en),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_rf_wr_en)
	);
	
	buffer rfwe3(
		.in(buf2_rf_wr_en),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf3_rf_wr_en)
	);
	
	buffer rfwe4(
		.in(buf3_rf_wr_en),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf4_rf_wr_en)
	);
	
	buffer5 rfwa2(
		.in(buf_rf_wr_addr),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_rf_wr_addr)
	);
	
	buffer5 rfwa3(
		.in(buf2_rf_wr_addr),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf3_rf_wr_addr)
	);
	
	buffer5 rfwa4(
		.in(buf3_rf_wr_addr),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf4_rf_wr_addr)
	);
	
	buffer32 dataout(
		.in(data_out),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_data_out)
	);
	
	buffer br1(
		.in(branch_reg),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_branch_reg)
	);
	
	buffer br2(
		.in(buf_branch_reg),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_branch_reg)
	);
	
	buffer br3(
		.in(buf2_branch_reg),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf3_branch_reg)
	);
	
	buffer br4(
		.in(buf3_branch_reg),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf4_branch_reg)
	);
	
	buffer upcc(
		.in(update_pc_comp),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_update_pc_comp)
	);
	
	buffer dmc(
		.in(data_mux_ctl),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf_data_mux_ctl)
	);
	
	buffer dmc1(
		.in(buf_data_mux_ctl),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf2_data_mux_ctl)
	);
	
	buffer bufferMcbufferface(
		.in(buf2_data_mux_ctl),
		.clock(CLOCK_50),
		.reset(!KEY[0]),
		.out(buf3_data_mux_ctl)
	);
	
	
endmodule

