module buffer32 (
	input [31:0] in,
	input clock, reset,
	output reg [31:0] out
);

always @(posedge clock or posedge reset)
begin
	if (reset) begin
		out <= 32'd0;
	end else begin
		out <= in;
	end
end

endmodule