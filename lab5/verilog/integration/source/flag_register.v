// When this gets a command to update the flags, it
// updates all the flags. Some of them may not apply
// well to a given operation, but it is assumed that
// the user knows what they're doing, and is only going
// to do a comparison when it makes sense to do so.

module flag_register (
	input update_flags,
	input z_flag,
	input n_flag,
	input c_flag,
	input v_flag,
	output reg [3:0] FLAGS
);

always @(*)
	if (update_flags)
		FLAGS = {z_flag, n_flag, c_flag, v_flag};
	else
		FLAGS = FLAGS;
		
endmodule
