module testBench;
	wire DO0, DO1, DO2, DO3, CLK, RST;

	ripple_down_counter UUT(DO0, DO1, DO2, DO3, CLK, RST);
	Test 				myTest(CLK, RST, DO0, DO1, DO2, DO3);

	initial 
		begin
			$dumpfile("ripple_down_counter.vcd");
			$dumpvars(0, UUT);
		end
endmodule


module DFlipFlop(q, qBar, D, clk, rst); 
	input D, clk, rst;
	output q, qBar;
	reg q;
	not n1 (qBar, q);
	always@ (negedge rst or posedge clk) begin
		if(!rst)
			q = 0;
		else
		q = D;
	end
endmodule

module ripple_down_counter(DO0, DO1, DO2, DO3, CLK, RST);
	output DO0, DO1, DO2, DO3;
	input CLK, RST;

	DFlipFlop DF0(DI0, DO0, DO0, CLK, RST);
	DFlipFlop DF1(DI1, DO1, DO1, DO0, RST);
	DFlipFlop DF2(DI2, DO2, DO2, DO1, RST);
	DFlipFlop DF3(DI3, DO3, DO3, DO2, RST);

endmodule

module Test (CLK, RST, DO0, DO1, DO2, DO3);
	output CLK, RST;
	input DO0, DO1, DO2, DO3;
	
	parameter delay = 1;
	parameter period = 1;

	reg CLK, RST;

	initial 
		begin
			CLK = 0;
			#delay RST = 1;
			#delay RST = 0;
			#delay RST = 1;
		end

	initial 
		begin
			$display("INPUTS \t\t OUTPUT, \t\t TIME");
			$display("DO0 DO1 DO2 DO3");
			$monitor(DO0, DO1, DO2, DO3);
			
			
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			
		end
endmodule