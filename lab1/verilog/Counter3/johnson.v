/*Module for a Johnson Counter*/
module johnson_counter(Q, CLK, RST);
	output [3:0] Q;
	input CLK, RST;
	reg [3:0] ns ,ps;

	assign Q = ps;
	
	/*Always block to move the system to the next state*/
	always@(posedge CLK)
		begin
			if(!RST)
				ps <= 4'b0000;			
			else 
				ps <= ns;
		end
	/*Case statements to determine what the next state will be*/
	always@(posedge CLK)
		begin 
			case(ps)
				4'b0000: ns = 4'b0001;
				4'b0001: ns = 4'b0011;
				4'b0011: ns = 4'b0111;
				4'b0111: ns = 4'b1111;
				4'b1111: ns = 4'b1110;
				4'b1110: ns = 4'b1100;
				4'b1100: ns = 4'b1000;
				4'b1000: ns = 4'b0000;
				default: ns = 4'b0000;
			endcase
		end
endmodule
/*Test Bench for counter*/
module testBench;
	wire CLK, RST;
	wire [3:0] Q;

	johnson_counter 		UUT(Q, CLK, RST);
	Test 					myTest(CLK, RST, Q);

	initial 
		begin
			$dumpfile("johnson.vcd");
			$dumpvars(0, UUT);
		end
endmodule
/*Running the test*/
module Test (CLK, RST, Q);
	output CLK, RST;
	input [3:0] Q;
	
	parameter delay = 1;
	parameter period = 1;

	reg CLK, RST;

	initial 
		begin
			CLK = 0;
			#delay RST = 1;
			#delay RST = 0;
			#delay RST = 1;
		end

	initial 
		begin
			$display("INPUTS \t\t OUTPUT, \t\t TIME");
			$display("DO0 DO1 DO2 DO3");
			$monitor(Q);
			
			
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			
		end
endmodule
