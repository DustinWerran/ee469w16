/*Module for a synchronous up counter*/
module synchronous_up_counter(d0, d1, d2, d3, CLK, RST);
	input CLK, RST;
	output d0, d1, d2, d3;
	wire dn0, dn1, dn2, dn3;
	/*The zeroth data bit and how the next value for the DFlip Flop is assigned*/
	assign dn0 = ~d0;	
	DFlipFlop DF0(d0, nil, dn0, CLK, RST);

	/*The first data bit and how the next value for the DFlip Flop is assigned*/
	assign dn1 = d0 ^ d1;
	DFlipFlop DF1(d1, nil, dn1, CLK, RST);

	
	/*The second data bit and how the next value for the DFlip Flop is assigned*/
	assign dn2 = (d0 & d1) ^ d2; 
	DFlipFlop DF2(d2, nil, dn2, CLK, RST);

	
	/*The third data bit and how the next value for the DFlip Flop is assigned*/
	assign dn3 = (d0 & d1 & d2) ^ d3; 
	DFlipFlop DF3(d3, nil, dn3, CLK, RST);


endmodule
/*Test Bench for the counter*/
module testBench;
	wire DO0, DO1, DO2, DO3, CLK, RST;

	synchronous_up_counter 	UUT(DO0, DO1, DO2, DO3, CLK, RST);
	Test 					myTest(CLK, RST, DO0, DO1, DO2, DO3);

	initial 
		begin
			$dumpfile("synchronous_up_counter.vcd");
			$dumpvars(0, UUT);
		end
endmodule

/*Given D Flip Flop code */
module DFlipFlop(q, qBar, D, clk, rst); 
	input D, clk, rst;
	output q, qBar;
	reg q;
	not n1 (qBar, q);
	always@ (negedge rst or posedge clk) begin
		if(!rst)
			q = 0;
		else
		q = D;
	end
endmodule

module Test (CLK, RST, DO0, DO1, DO2, DO3);
	output CLK, RST;
	input DO0, DO1, DO2, DO3;
	
	parameter delay = 1;
	parameter period = 1;

	reg CLK, RST;

	initial 
		begin
			CLK = 0;
			#delay RST = 1;
			#delay RST = 0;
			#delay RST = 1;
		end

	initial 
		begin
			$display("INPUTS \t\t OUTPUT, \t\t TIME");
			$display("DO0 DO1 DO2 DO3");
			$monitor(DO0, DO1, DO2, DO3);
			
			
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			#period CLK = ~CLK;
			
		end
endmodule