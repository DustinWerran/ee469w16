module ALU_behavioral (
	input [31:0] BusA, 
	input [31:0] BusB,
	input [2:0] Control,
	output reg [31:0] OutBus,
	output reg Zero, Overflow, CarryOut, Negative
);

localparam NOP = 3'b000,
			  ADD = 3'b001,
			  SUB = 3'b010,
			  AND = 3'b011,
			  OR  = 3'b100,
			  XOR = 3'b101,
			  LSL = 3'b110;

wire [16:0] BusAExtend, BusBExtend;
reg [16:0] PreOut;

assign BusAExtend = {BusA[15], BusA[15:0]};
assign BusBExtend = {BusB[15], BusB[15:0]};

always @(*) begin
	PreOut = 17'bX;
	case (Control)
		NOP: begin end
		ADD: PreOut = BusAExtend + BusBExtend;
		SUB: PreOut = BusBExtend + (~BusAExtend + 1'b1);
		AND: PreOut = BusA & BusB;
		OR: PreOut = BusA | BusB;
		XOR: PreOut = BusA ^ BusB;
		LSL: 	begin
						if (BusB == 32'd1) begin
							PreOut = {BusAExtend[15:0], 1'b0};
						end else if (BusB == 32'd2) begin
							PreOut = {BusAExtend[14:0], 2'b0};
						end else if (BusB == 32'd3) begin
							PreOut = {BusAExtend[13:0], 3'b0};
						end else begin
							PreOut = BusAExtend;
						end
					end
		default: PreOut = 17'd0;
	endcase
	
	if (PreOut == 17'd0) begin
		Zero = 1'b1;
	end else begin
		Zero = 1'b0;
	end
	if (PreOut[15]) begin
		Negative = 1'b1;
	end else begin
		Negative = 1'b0;
	end
	if (PreOut[16] ^ PreOut[15]) begin
		Overflow = 1'b1;
	end else begin
		Overflow = 1'b0;
	end
	if (BusAExtend[16] ^ BusBExtend[16]) begin
		CarryOut = ~PreOut[16];
	end else begin
		CarryOut = PreOut[16];
	end
	if (PreOut[15]) begin
		OutBus = {{16{1'b1}}, PreOut[15:0]};
	end else begin
		OutBus = {{16{1'b0}}, PreOut[15:0]};
	end
end

endmodule
/*
module testbench;

	wire Zero, CarryOut, Overflow, Negative;
	wire [31:0] BusA, BusB, OutBus;
	wire [2:0] Control;
	
	ALU_behavioral uut(BusA, BusB, Control, OutBus, Zero, Overflow, CarryOut, Negative);
	Test mytest(BusA, BusB, Control, OutBus, Zero, Overflow, CarryOut, Negative);
	initial
		begin
			$dumpfile("ALU_BEHAVIORAL.vcd");
			$dumpvars(0, uut, mytest);
		end
endmodule

module Test (
	output reg [31:0] BusA,
	output reg [31:0] BusB,
	output reg [2:0] Control,
	input [31:0] OutBus,
	input Zero, Overflow, Carryout, Negative
);

parameter d = 20;

initial
begin

	BusA = 32'd0;
	BusB = 32'd0;
	Control = 3'd0;
	#d Control = 3'b001;
	#d BusA = 32'd10;
	BusB = 32'b11111111111111111111111111111011;
	Control = 3'b001;
	#d Control = 3'b010;
	#d Control = 3'b011;
	#d Control = 3'b100;
	#d Control = 3'b101;
	#d Control = 3'b110;
	BusB = 32'd1;
	#d BusB = 32'd2;
	#d BusB = 32'd3;
	#d BusA = 32'b00000000000000000111111111111111;
	Control = 3'b001;
	#d BusA = 32'd0;
	$finish;
end

endmodule
*/