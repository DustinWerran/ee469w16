//+`include "SRAM32.v"
`include "ALU_behavioral.v"

module integration_alu(	
	LEDR[9:0], 
	KEY[3:0],
	CLOCK_50,
	SW[9:0],
	HEX3[6:0],
	HEX2[6:0],
	HEX1[6:0],
	HEX0[6:0]
);
	output [9:0] LEDR;
	input [3:0] KEY;
	input CLOCK_50;
	input [9:0] SW;
	output [6:0] HEX3, HEX2, HEX1, HEX0;



	wire [2:0] control;
	wire A_nB;
	wire reset;
	wire clock;
	wire enter;
	wire run;
	reg [3:0] A_storage [0:3];
	reg [3:0] B_storage [0:3];
	wire [2:0] ps;
	reg [2:0] psA, psB;
	reg [31:0] BusA_reg, BusB_reg;
	reg [2:0] Control_reg;
	reg [31:0] OutBus_reg; 
	wire [31:0] OutBUS_reg_waiting;
	wire Zero, Overflow, CarryOut, Negative;
	wire display;
	wire [31:0] datainA, datainB;
	assign display = SW[9];
	assign ps[2] = SW[8];
	assign ps[1:0] = SW[8] ? psA : psB;
	assign control = SW[6:4];
	assign reset = !KEY[3];
	assign clock = CLOCK_50;
	assign Zero = LEDR[0];
	assign Overflow = LEDR[1];
	assign CarryOut = LEDR[2];
	assign Negative = LEDR[3];
	assign A_nB = SW[8];

	assign enter = !KEY[0];
	assign run = !KEY[1];
	reg [6:0] HEX0, HEX1, HEX2,HEX3;

	assign datainA = {
		16'b0, A_storage[3], 
		A_storage[2], A_storage[1], 
		A_storage[0]
		};
	assign datainB = { 
		16'b0, B_storage[3], 
		B_storage[2], B_storage[1], 
		B_storage[0]
		};


	ALU_behavioral ALU(
	.BusA(datainA), 
	.BusB(datainB),
	.Control(control),
	
	.OutBus(OutBUS_reg_waiting),
	.Zero(Zero), .Overflow(Overflow), .CarryOut(carryOut), .Negative(Negative)
);
	always@(posedge run) begin
		OutBus_reg <= OutBUS_reg_waiting;
	end

	always@(posedge enter) begin
		if(display) begin
			
		end else begin
			if(A_nB) begin
				A_storage[psA] <= SW[3:0];
				psA <= psA + 1'b1;	
			end else begin
				B_storage[psB] <= SW[3:0];
				psB <= psB + 1'b1;
			end
		end
	end

	always@(posedge clock or posedge reset) begin
		if(reset) begin
			OutBus_reg <= 0;
			A_storage[0] <= 0;
			A_storage[1] <= 0;
			A_storage[2] <= 0;
			A_storage[3] <= 0;
			B_storage[0] <= 0;
			B_storage[1] <= 0;
			B_storage[2] <= 0;
			B_storage[3] <= 0;
			psA <= 0;
			psB <= 0;
		end else begin
			if (display) begin
				HEX0 <= HEX(OutBus_reg[3:0]);
				HEX1 <= HEX(OutBus_reg[7:4]);
				HEX2 <= HEX(OutBus_reg[11:8]);
				HEX3 <= HEX(OutBus_reg[15:12]);
			end else begin
				
				case(ps) 
					3'd0: begin 
						HEX0 <= HEX(SW[3:0]);
						HEX1 <= HEX(A_storage[1]);
						HEX2 <= HEX(A_storage[2]);
						HEX3 <= HEX(A_storage[3]);
					end
					3'd1: begin	
						HEX0 <= HEX(A_storage[0]);
						HEX1 <= HEX(SW[3:0]);
						HEX2 <= HEX(A_storage[2]);
						HEX3 <= HEX(A_storage[3]);
					end
					3'd2: begin	
						HEX0 <= HEX(A_storage[0]);
						HEX1 <= HEX(A_storage[1]);
						HEX2 <= HEX(SW[3:0]);
						HEX3 <= HEX(A_storage[3]);
					end
					3'd3: begin	
						HEX0 <= HEX(A_storage[0]);
						HEX1 <= HEX(A_storage[1]);
						HEX2 <= HEX(A_storage[3]);
						HEX3 <= HEX(SW[3:0]);
					end
					
				
					3'd4: begin 
						HEX0 <= HEX(SW[3:0]);
						HEX1 <= HEX(B_storage[1]);
						HEX2 <= HEX(B_storage[2]);
						HEX3 <= HEX(B_storage[3]);
					end
					3'd5: begin	
						HEX0 <= HEX(B_storage[0]);
						HEX1 <= HEX(SW[3:0]);
						HEX2 <= HEX(B_storage[2]);
						HEX3 <= HEX(B_storage[3]);
					end
					3'd6: begin	
						HEX0 <= HEX(B_storage[0]);
						HEX1 <= HEX(B_storage[1]);
						HEX2 <= HEX(SW[3:0]);
						HEX3 <= HEX(B_storage[3]);
					end
					3'd7: begin	
						HEX0 <= HEX(B_storage[0]);
						HEX1 <= HEX(B_storage[1]);
						HEX2 <= HEX(B_storage[2]);
						HEX3 <= HEX(SW[3:0]);
					end
				endcase
			end
		end
	end
	
	
	function [6:0] HEX;
		input [3:0] BIN;
		case(BIN)      //6543210 
			4'd0:	HEX = 7'b1000000;
			4'd1:	HEX = 7'b1111001;
			4'd2: 	HEX = 7'b0100100;
			4'd3:	HEX = 7'b0110000;
			4'd4:	HEX = 7'b0011001;
			4'd5:	HEX = 7'b0010010;
			4'd6:	HEX = 7'b0000010;
			4'd7:	HEX = 7'b1111000;
			4'd8:	HEX = 7'b0000000;
			4'd9:	HEX = 7'b0010000;
			4'd10:	HEX = 7'b0001000;
			4'd11:	HEX = 7'b0000011;
			4'd12:	HEX = 7'b1000110;
			4'd13:	HEX = 7'b0100001;
			4'd14:	HEX = 7'b0000110;
			4'd15:	HEX = 7'b0001110;
			default: HEX = 7'b1000000;
		endcase



	endfunction
endmodule

module testbench;

	wire [9:0] LEDR;
	wire [3:0] KEY;
	reg CLOCK_50;
	wire [9:0] SW;
	wire [6:0] HEX0, HEX1, HEX2, HEX3;
	reg reset;
	reg enter;
	reg run;
	reg A_nB;
	reg display;
	reg [3:0] value;
	reg[2:0] control;
	assign KEY[3] = !reset;
	assign KEY[1] = !run;
	assign KEY[0] = !enter;
	assign SW[6:4] = control;
	assign SW[3:0] = value;
	assign SW[8] = A_nB;
	assign SW[9] = display;
	assign SW[7] = 0;
	assign KEY[2] = 0;

	integration_alu DUT(	
	LEDR[9:0], 
	KEY[3:0],
	CLOCK_50,
	SW[9:0],
	HEX3[6:0],
	HEX2[6:0],
	HEX1[6:0],
	HEX0[6:0]
);

	always begin
		#2 CLOCK_50 = ~CLOCK_50;
	end

	initial begin
		$dumpfile("ALU_board_part1.vcd");
		$dumpvars(0, DUT);
		CLOCK_50 = 0;
		reset = 1;
		run = 0;
		enter = 0;
		display = 0;
		value = 0;
		A_nB = 0;
		control = 1;
		value = 0;

		#10;
		reset = 0;
		
		#4;
		enter = 1;
		value = 1;
		#4;
		enter = 0;
		#4;
		enter = 1;
		#4;
		enter = 0;
		#4;
		enter = 1;
		#4;
		enter = 0;
		value = 1;
		#4;
		enter = 1;
		#4;
		enter = 0;
		A_nB = 1;
		value = 1;
		#4;
		enter = 1;
		#4;
		enter = 0;
		#4;
		enter = 1;
		#4;
		enter = 0;
		#4;
		enter = 1;
		#4;
		enter = 0;
		display = 1;
		run = 1;
		#10;

		$finish;





	end
endmodule
