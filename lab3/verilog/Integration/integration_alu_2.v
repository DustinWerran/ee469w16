

module integration_alu_2(	
	LEDR[9:0], 
	KEY[3:0],
	CLOCK_50,
	SW[9:0],
	HEX3[6:0],
	HEX2[6:0],
	HEX1[6:0],
	HEX0[6:0]
);

	output [9:0] LEDR;
	input [3:0] KEY;
	input CLOCK_50;
	input [9:0] SW;
	output [6:0] HEX3, HEX2, HEX1, HEX0;

	wire	[4:0] 	REG_WRITE_ADDRESS, REG_READ_ADDRESS_2, REG_READ_ADDRESS_1;
	wire			REG_OE1;
	wire			REG_OE2;
	wire			SRAM_CONTROL;
	wire	[10:0]	SRAM_ADDRESS;
	wire			SRAM_WE, SRAM_OE;
	wire			DATA_CONTROL;
	wire			REG_WE;
	//wire 			CLOCK;
	wire  			RESET;
	wire 	[31:0] 	SRAM_DATA;
	wire 	[31:0] 	ALU_WRITE_DATA, REG_READ_DATA1, REG_READ_DATA2;
	wire 	[31:0] 	DATA_LINE;
	wire  Zero, Overflow, CarryOut, Negative;
	wire  Zero0, Overflow0, CarryOut0, Negative0;

	assign RESET = !KEY[0];
	assign DATA_LINE = DATA_CONTROL ? ALU_WRITE_DATA : SRAM_DATA;
	assign Zero = DATA_CONTROL && Zero0;
	assign Overflow = DATA_CONTROL && Overflow0;
	assign CarryOut = DATA_CONTROL && CarryOut0;
	assign Negative = DATA_CONTROL && Negative0;
	
	//assign DATA = DATA_CONTROL ? 
	
	driver mydriver(
		SRAM_ADDRESS,
		SRAM_WE,
		SRAM_OE,
		REG_WRITE_ADDRESS,
		REG_READ_ADDRESS_1,
		REG_READ_ADDRESS_2,
		DATA_CONTROL,
		REG_OE1,
		REG_OE2,
		REG_WE,
		CLOCK_50,
		SRAM_DATA,
		RESET,

	);

	SRAM32 mysram (
		.DATA(SRAM_DATA), 
		.ADDRESS(SRAM_ADDRESS), 
		.CLK(CLOCK_50), 
		.nWE(SRAM_WE), 
		.OE(SRAM_OE), 
		.RST(RESET)
		);

	alu myalu(
	.A(REG_READ_DATA1), 
	.B(REG_READ_DATA2),
	.ctl(SRAM_DATA),
	.result(ALU_WRITE_DATA),
	.Z(Zero0), .N(Negative0), .C(CarryOut0), .V(Overflow0)
	);

	Regfile myregfile(
	.Reg1ReadData(REG_READ_DATA1), .Reg2ReadData(REG_READ_DATA2),
	.Reg1ReadSelect(REG_READ_ADDRESS_1), .Reg2ReadSelect(REG_READ_ADDRESS_2),
	.WriteRegSelect(REG_WRITE_ADDRESS),
	.WriteEnable(REG_WE),
	.WriteRegData(DATA_LINE),
	.clock(CLOCK_50), 
	.reset(RESET)
	);


endmodule

module driver(
		SRAM_ADDRESS,
		SRAM_WE,
		SRAM_OE,
		REG_WRITE_ADDRESS,
		REG_READ_ADDRESS_1,
		REG_READ_ADDRESS_2,
		DATA_CONTROL,
		REG_OE1,
		REG_OE2,
		REG_WE,
		CLOCK,
		SRAM_DATA,
		RESET,

	);
	output [4:0] REG_WRITE_ADDRESS, REG_READ_ADDRESS_2, REG_READ_ADDRESS_1;
	output REG_OE1;
	output REG_OE2;
	//output SRAM_CONTROL;
	output [10:0] SRAM_ADDRESS;
	output SRAM_WE, SRAM_OE;
	output DATA_CONTROL;
	output REG_WE;
	input CLOCK;
	input RESET;
	
	inout [31:0] SRAM_DATA;
	wire OE; 

	reg [31:0] SRAM_DATA_OUT;
	wire [31:0] SRAM_DATA_IN;
	reg [6:0] ps;
	reg DATA_CONTROL;
	reg [4:0] REG_WRITE_ADDRESS, REG_READ_ADDRESS_2, REG_READ_ADDRESS_1;
	reg REG_OE1;
	reg REG_OE2;
	reg REG_WE;

	reg [10:0] SRAM_ADDRESS;
	reg SRAM_WE, SRAM_OE;

	assign OE = SRAM_OE;
	assign SRAM_DATA_IN = SRAM_DATA;
	assign SRAM_DATA = OE ? 32'bz : SRAM_DATA_OUT;


	always@(posedge CLOCK or posedge RESET) begin
		if(RESET) begin
			SRAM_ADDRESS <= 0;
			SRAM_DATA_OUT <= 0;
			SRAM_WE <= 0;
			SRAM_OE <= 0;
			ps <= 0;

			DATA_CONTROL <= 0;
		end else begin
			/*SETUP*/
			if(!ps[6]) begin
				SRAM_ADDRESS <= ps;
				SRAM_WE <= ps[0];
				case(ps[5:1])
					//----------//  ZERO
					5'd0: SRAM_DATA_OUT <= 6;  	//DATA A
					5'd1: SRAM_DATA_OUT <= 10;	//DATA B
					5'd2: SRAM_DATA_OUT <= 1;	//CONTROL
					//----------// NEGATIVE SUBTRACTION
					5'd4: SRAM_DATA_OUT <= 8;
					5'd5: SRAM_DATA_OUT <= 6;
					5'd6: SRAM_DATA_OUT <= 2;
					//----------//  AND OPERATION
					5'd8: SRAM_DATA_OUT <= 8;
					5'd9: SRAM_DATA_OUT <= 7;
					5'd10: SRAM_DATA_OUT <= 3;
					//----------//  OR OPERATION
					5'd12: SRAM_DATA_OUT <= 24;
					5'd13: SRAM_DATA_OUT <= 12;
					5'd14: SRAM_DATA_OUT <= 4;
					//----------//  EOR
					5'd16: SRAM_DATA_OUT <= 21;
					5'd17: SRAM_DATA_OUT <= 24;
					5'd18: SRAM_DATA_OUT <= 5;
					//----------// LSL OPERATION
					5'd20: SRAM_DATA_OUT <= 32'd120;
					5'd21: SRAM_DATA_OUT <= 3;
					5'd22: SRAM_DATA_OUT <= 6;
					//----------// OVERFLOW
					5'd24: SRAM_DATA_OUT <= 32'h00007FFF;
					5'd25: SRAM_DATA_OUT <= 32'd1;
					5'd26: SRAM_DATA_OUT <= 1;
					default: SRAM_DATA_OUT <= 0;
				endcase
			end

			if(ps[6]) begin
				REG_OE1 <= 1;
				REG_OE2 <= 1;
				SRAM_WE <= 1;
				SRAM_ADDRESS <= (ps[5:1] << 1);
				SRAM_OE <= 1;
				REG_WE <= !ps[0];
				REG_WRITE_ADDRESS <= ps[5:1]; 
				REG_READ_ADDRESS_1 <= ps[5:1] - 4'd2;
				REG_READ_ADDRESS_2 <= ps[5:1] - 4'd1;
				DATA_CONTROL <= ps[2] && ps[1];
			end

			 

		ps <= ps + 1;
		end
		

	end


endmodule 

module testbench_integration;

	wire [9:0] LEDR;
	wire [3:0] KEY;
	reg CLOCK_50;
	wire [9:0] SW;
	wire [6:0] HEX0, HEX1, HEX2, HEX3;
	reg RESET;
	wire Zero, CarryOut, Negative, Overflow;
	
	integration_alu_2 DUT(	
	LEDR[9:0], 
	KEY[3:0],
	CLOCK_50,
	SW[9:0],
	HEX3[6:0],
	HEX2[6:0],
	HEX1[6:0],
	HEX0[6:0]
);
	assign KEY[0] = !RESET;


	always begin
		#2CLOCK_50 = ~CLOCK_50;
	end
	initial begin
		$dumpfile("ALU_INTEG.vcd");
		$dumpvars(0, DUT);
		CLOCK_50 = 0;
		#10;
		RESET = 1;
		#10;
		RESET = 0;

		#1000;
		$finish;
	end


endmodule