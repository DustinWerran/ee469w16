vlib work

vlog -work work ../SRAM32.v

vsim -t 1ps -novopt SRAM32_tb

view signals
view wave

do wave_SRAM32.do

run -all