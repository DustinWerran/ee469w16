`include "32SRAM.v"

module driver(	
	LEDR[9:0], 
	KEY[3:0],
	CLOCK_50,
	SW[9:0]
	HEX3[6:0],
	HEX2[6:0],
	HEX1[6:0],
	HEX0[6:0]
);
	wire A_nB
	wire reset;
	wire clock;
	wire enter;
	wire run;
	reg [3:0] A_storage [0:3];
	reg [3:0] B_storage [0:3];
	reg[1:0] psA, psB;
	reg [31:0] BusA_reg, BusB_reg;
	reg [2:0] Control_reg;
	reg [31:0] OutBus_reg
	wire Zero, Overflow, CarryOut, Negative;
	reg display;
	assign display = SW[9];
	ALU_behavioral ALU(
	input [31:0] BusA, 
	input [31:0] BusB,
	input [2:0] Control,
	input clock,
	output [31:0] OutBus,
	output Zero, Overflow, CarryOut, Negative
);

	always(@posedge enter) begin
		if(display) begin
			

		end else begin
			if(A_nB) begin
				A_storage[psA] <= SW[0:3];
				psA <= psA + 1'b1;	
			end else if(!B_ready) begin
				B_storage[psB] <= SW[0:3];
				psB <= psB + 1'b1;
			end
		end
	end
	always(@posedge clock or posedge reset) begin
		if(reset) begin
			//Reset Functions
		end else begin
			if(A_nB) begin
				case(psA) begin
					2'd0: begin 
						HEX0 <= HEX(SW[3:0]);
						HEX1 <= A_storage[1];
						HEX2 <= A_storage[2];
						HEX3 <= A_storage[3];
					end
					2'd1: begin	
						HEX0 <= A_storage[0];
						HEX1 <= HEX(SW[3:0]);
						HEX2 <= A_storage[2];
						HEX3 <= A_storage[3];
					end
					2'd2: begin	
						HEX0 <= A_storage[0];
						HEX1 <= A_storage[1];
						HEX2 <= HEX(SW[3:0]);
						HEX3 <= A_storage[3];
					end
					2'd3: begin	
						HEX0 <= A_storage[0];
						HEX1 <= A_storage[1];
						HEX2 <= A_storage[3];
						HEX3 <= HEX(SW[3:0]);
					end
				endcase
			end else begin
				case(psA) begin
					2'd0: begin 
						HEX0 <= HEX(SW[3:0]);
						HEX1 <= B_storage[1];
						HEX2 <= B_storage[2];
						HEX3 <= B_storage[3];
					end
					2'd1: begin	
						HEX0 <= B_storage[0];
						HEX1 <= HEX(SW[3:0]);
						HEX2 <= B_storage[2];
						HEX3 <= B_storage[3];
					end
					2'd2: begin	
						HEX0 <= B_storage[0];
						HEX1 <= B_storage[1];
						HEX2 <= HEX(SW[3:0]);
						HEX3 <= B_storage[3];
					end
					2'd3: begin	
						HEX0 <= B_storage[0];
						HEX1 <= B_storage[1];
						HEX2 <= B_storage[3];
						HEX3 <= HEX(SW[3:0]);
					end
				endcase
			end
		end
	end
	assign reset = !KEY[3];
	assign clock = CLOCK_50;

	assign enter = !KEY[0];
	assign run = !KEY[1];

	function HEX[6:0];

		input [3:0] BIN;

		

		case(BIN) begin     //6543210 
			4'd0:	HEX <= 7'b1000000;
			4'd1:	HEX <= 7'b1111001;
			4'd2: 	HEX <= 7'b0100100;
			4'd3:	HEX <= 7'b0110000;
			4'd4:	HEX <= 7'b0011001;
			4'd5:	HEX <= 7'b0010010;
			4'd6:	HEX <= 7'b0000010;
			4'd7:	HEX <= 7'b1111000;
			4'd8:	HEX <= 7'b0000000;
			4'd9:	HEX <= 7'b0010000;
			4'd10:	HEX <= 7'b0001000;
			4'd11:	HEX <= 7'b0000011;
			4'd12:	HEX <= 7'b1000110;
			4'd13:	HEX <= 7'b0100001;
			4'd14:	HEX <= 7'b0000110;
			4'd15:	HEX <= 7'b0001110;
		endcase



	endfunction
endmodule


