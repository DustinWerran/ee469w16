vlib work

vlog -work work ../source/black_cell.v
vlog -work work ../source/grey_cell.v
vlog -work work ../source/half_adder.v
vlog -work work ../source/kogge_stone.v
vlog -work work kogge_stone_tb.v

vsim -t 10ps -novopt kogge_stone_tb

view signals
view wave

do wave_kogge_stone.do

run 1us