`timescale 1ns/10ps

module kogge_stone_tb ();

reg [16:0] A, B;
reg Ci;
wire [15:0] S;
wire V, c;

kogge_stone uut (.A(A[15:0]), .B(B[15:0]), .Ci(Ci), .S(S), .V(V), .C(C));

parameter delay = 1;

initial begin
	A <= 17'b0;
	B <= 17'b0;
	Ci <= 1'b0;
end

always #(delay) begin
	if ((A[15] && B[15] && ~S[15]) || (~A[15] && ~B[15] && S[15])) begin
		if (!V) $display($time, " overflow flag error");
	end
	if (A + B + Ci > 17'h0FFFF) begin
		if (!C) $display($time, " carry flag error");
		if (A + B + Ci != {1'b1, S}) $display($time," overflow addition error");
	end
	else if (A + B + Ci != S) $display($time, " addition error");
	
	A[15:0] <= $urandom;
	B[15:0] <= $urandom;
	Ci <= $urandom;
end

endmodule
