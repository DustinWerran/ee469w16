vlib work

vlog -work work ../source/left_shift_logic.v

vsim -t 1ps -novopt left_shift_logic_tb

view signals
view wave

do wave_left_shift_logic.do

run 100 nS