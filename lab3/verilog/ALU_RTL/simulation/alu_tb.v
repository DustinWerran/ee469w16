`timescale 1ns/10ps

module alu_tb ();

reg [31:0] A, B;
reg [2:0] ctl;

wire [31:0] result;
wire Z, N, C, V;

parameter delay = 1;

localparam NOP = 3'b000,
			  ADD = 3'b001,
			  SUB = 3'b010,
			  AND = 3'b011,
			  OR  = 3'b100,
			  XOR = 3'b101,
			  LSL = 3'b110;

alu uut (
	.A(A),
	.B(B),
	.ctl(ctl),
	.result(result),
	.Z(Z), .N(N), .C(C), .V(V)
);

initial begin
	A <= 32'b0;
	B <= 32'b0;
	ctl <= 3'b001;  // test Z-detect
end

always #(delay) begin
	case (ctl)
		ADD: if (result[15:0] != A[15:0] + B[15:0])
			$display($time, " bad addition");
		SUB: if (result[15:0] != B[15:0] - A[15:0])
			$display($time, " bad subtraction");
//		AND: if (result[15:0] != A[15:0] & B[15:0])
//			 $display($time, " bad AND");
//		OR:  if (result[15:0] != A[15:0] | B[15:0])
//			 $display($time, " bad OR");
//		XOR: if (result[15:0] != A[15:0] ^ B[15:0])
//			 $display($time, " bad XOR");
		LSL: if (result[15:0] != A[15:0] << B[1:0])
			$display($time, " bad LSL");
	endcase
	
	A <= $urandom;
	B <= $urandom;
	ctl <= $urandom%7;
end

endmodule
	