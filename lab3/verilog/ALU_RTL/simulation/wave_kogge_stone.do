onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /kogge_stone_tb/A
add wave -noupdate -radix hexadecimal /kogge_stone_tb/B
add wave -noupdate -radix hexadecimal /kogge_stone_tb/Ci
add wave -noupdate -radix hexadecimal /kogge_stone_tb/S
add wave -noupdate -radix hexadecimal /kogge_stone_tb/V
add wave -noupdate -radix hexadecimal /kogge_stone_tb/C
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/P0
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/G0
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/P1
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/G1
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/P2
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/G2
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/P3
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/G3
add wave -noupdate -expand -group {Interior Signals} -radix hexadecimal /kogge_stone_tb/uut/carries
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {69620 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 228
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {998260 ps} {1000100 ps}
