vlib work

vlog -work work ../source/add_sub.v
vlog -work work ../source/kogge_stone.v
vlog -work work ../source/half_adder.v
vlog -work work ../source/black_cell.v
vlog -work work ../source/grey_cell.v

vlog -work work ../source/left_shift_logic.v

vlog -work work ../source/alu.v
vlog -work work alu_tb.v

vsim -t 10ps -novopt alu_tb

view signals
view wave

do wave_alu.do

run 100 nS