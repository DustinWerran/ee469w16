module grey_cell #(
	parameter width = 16
) (
	input [width-1:0] Gitok,
	input [width-1:0] Pitok,
	input [width-1:0] Gkminusonetoj,
	output reg [width-1:0] Gitoj
);

integer i;

always @(*) begin
	for (i = 0; i < width; i = i + 1) begin
		Gitoj[i] = Gitok[i] | (Pitok[i] & Gkminusonetoj[i]);
	end
end

endmodule
