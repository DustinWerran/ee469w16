module half_adder #(
	parameter width = 16
) (
	input [width-1:0] A,
	input [width-1:0] B,
	output reg [width-1:0] G,
	output reg [width-1:0] P
);

integer i;

always @(*) begin
	for (i = 0; i < width; i = i + 1) begin
		G = A & B;
		P = A ^ B;
	end
end

endmodule
