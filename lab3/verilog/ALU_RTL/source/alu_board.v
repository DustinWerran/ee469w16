module alu_board (
	input [3:0] KEY,
	input [9:0] SW,
	output [6:0] HEX0,
	output [6:0] HEX1,
	output [6:0] HEX2,
	output [6:0] HEX3
);

reg [3:0] bcd_0, bcd_1, bcd_2, bcd_3;
wire [31:0] result;

wire RST, ENTER, RUN;

assign RST   = ~KEY[2];
assign ENTER = ~KEY[0];
assign RUN   = ~KEY[1];

seg7 hex0 (.bcd(bcd_0), .leds_inv(HEX0));
seg7 hex1 (.bcd(bcd_1), .leds_inv(HEX1));
seg7 hex2 (.bcd(bcd_2), .leds_inv(HEX2));
seg7 hex3 (.bcd(bcd_3), .leds_inv(HEX3));

reg [31:0] A, B;
reg [2:0] ctl;

always @(*) begin

	if (RST) begin
		A = 32'b0;
		B = 32'b0;
		ctl = 3'b0;
		{bcd_3, bcd_2, bcd_1, bcd_0} = 16'b0;
	end
	
	else if (ENTER) begin
		if (!SW[8]) begin
			A = {{12{SW[3]}}, SW[3:0]};
		end 
		else begin
			B = {{12{SW[3]}}, SW[3:0]};
		end
	end
	
	else if (RUN) begin
		ctl = SW[6:4];
		if (SW[9]) begin
			{bcd_3, bcd_2, bcd_1, bcd_0} = result[15:0];
		end
	end
	
	if (!SW[9]) begin
		if (!SW[8]) begin
			{bcd_3, bcd_2, bcd_1, bcd_0} = A[15:0];
		end
		else begin
			{bcd_3, bcd_2, bcd_1, bcd_0} = B[15:0];
		end
	end
	
end

alu compute (
	.A(A),
	.B(B),
	.ctl(ctl),
	.result(result),
	.Z(), .N(), .C(), .V()
);

endmodule
