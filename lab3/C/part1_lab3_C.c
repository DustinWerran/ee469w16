
#include <stdio.h>

int main(void) {
	int a;
	int b;
	float c;
	float d;
	char e;
	char f;

	int* g;
	float* h;
	char* i;

	g = &a;
	printf("The address location of a is %p\n", g);
	g = &b;
	printf("The address location of b is %p\n", g);
	h = &c;
	printf("The address location of c is %p\n", h);
	h = &d;
	printf("The address location of d is %p\n", h);
	i = &e;
	printf("The address location of e is %p\n", i);
	i = &f;
	printf("The address location of f is %p\n", i);

	return 0;
}