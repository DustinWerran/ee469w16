/*C Program involving  pointers*/
#include <stdio.h>


int main(void) {
	/*Integer Values*/
	int A = 25;
	int B = 16;
	int C = 7;
	int D = 4;
	int E = 9;
	int result;
	/*Pointer Objects*/
	int* PtrA = &A;
	int* PtrB = &B;
	int* PtrC = &C;
	int* PtrD = &D;
	int* PtrE = &E;
	/*Operation*/
	result = ((*PtrA - *PtrB) * (*PtrC + *PtrD)) / *PtrE;
	printf("%d\n", result);

	return 0;
}