onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/CLOCK_50
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/KEY
add wave -noupdate -radix binary /integration_single_cycle_tb/uut/im_read_data
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/data_out
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/z_flag
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/n_flag
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/c_flag
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/v_flag
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/ALU_ctl
add wave -noupdate -radix decimal /integration_single_cycle_tb/uut/im_read_address
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/data_mux_ctl
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/alu_op0_from_id
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/alu_op1_from_id
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/sel_alu_op0
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/sel_alu_op1
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/rf_rd_addr_0
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/rf_rd_addr_1
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/rf_wr_addr
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/rf_wr_en
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/nWE
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/OE
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/bus_b
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/bus_a
add wave -noupdate -radix decimal /integration_single_cycle_tb/uut/sram_out
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/sram_write
add wave -noupdate -radix decimal /integration_single_cycle_tb/uut/alu_result
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/alu_a
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/alu_b
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/alu_op0_id_extend
add wave -noupdate -radix hexadecimal /integration_single_cycle_tb/uut/alu_op1_id_extend
add wave -noupdate /integration_single_cycle_tb/uut/im/instrmem
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {233002 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 292
configure wave -valuecolwidth 157
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {189750 ps} {268500 ps}
