vlib work

vlog -work work ../source/SRAM32.v

vlog -work work ../source/Decoder5to32.v
vlog -work work ../source/Decoder2to4.v
vlog -work work ../source/3to8Decoder.v
vlog -work work ../source/Bitcell.v
vlog -work work ../source/DFlipFlop.v
vlog -work work ../source/Regfile.v

vlog -work work ../source/add_sub.v
vlog -work work ../source/kogge_stone.v
vlog -work work ../source/half_adder.v
vlog -work work ../source/black_cell.v
vlog -work work ../source/grey_cell.v
vlog -work work ../source/left_shift_logic.v
vlog -work work ../source/alu.v

vlog -work work ../source/instruction_memory.v
vlog -work work ../source/control.sv
vlog -work work ../source/mux2to1_32bit.v
vlog -work work ../source/integration_single_cycle.v

vlog -work work ../source/integration_single_cycle_tb.v


vsim -t 1ps -novopt integration_single_cycle_tb

view signals
view wave

do wave_integration_single_cycle.do

run 300 nS