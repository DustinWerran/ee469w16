`timescale 1ns/10ps

// just a simple wrapper for the adder
// flips the bits of OP0

module add_sub (
	input [15:0] A, B,
	input sub,
	output [15:0] result,
	output C, V
);

reg [15:0] A_temp;

// flip the bits if needed
integer i;
always @(*) begin
	for (i = 0; i < 16; i = i + 1) begin
		A_temp[i] = A[i] ^ sub;
	end
end

kogge_stone adder (
	.A(A_temp), 
	.B(B),
	.Ci(sub),
	.S(result),
	.C(C), .V(V)
);

endmodule

