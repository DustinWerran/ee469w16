module alu (
	input [31:0] A, B,
	input [2:0] ctl,
	output [31:0] result,
	output reg Z, N, C, V
);

localparam NOP = 3'b000,
			  ADD = 3'b001,
			  SUB = 3'b010,
			  AND = 3'b011,
			  OR  = 3'b100,
			  XOR = 3'b101,
			  LSL = 3'b110;
			  
reg [15:0] result_temp;

reg sub;

wire [15:0] add_sub_out;
wire [15:0] shift_out;
wire add_sub_C, add_sub_V;

add_sub as (
	.A(A[15:0]),
	.B(B[15:0]),
	.sub(sub),
	.result(add_sub_out),
	.C(add_sub_C),
	.V(add_sub_V)
);

left_shift_logic shifter (
	.A(A[15:0]),
	.shift_amt(B[1:0]),
	.result(shift_out)
);

// result mux
always @(*) begin

	// defaults, prevent latches
	result_temp = 16'bX;
	sub = 1'bx;
	Z = 1'bx;
	N = 1'bx;
	C = 1'bx;
	V = 1'bx;
	
	case (ctl)
		
		ADD: begin
			sub = 1'b0;
			result_temp = add_sub_out;
			Z = !(|result_temp);
			N = result_temp[15];
			C = add_sub_C;
			V = add_sub_V;
		end
		
		SUB: begin
			sub = 1'b1;
			result_temp = add_sub_out;
			Z = !(|result_temp);
			N = result_temp[15];
			C = add_sub_C;
			V = add_sub_V;
		end
		
		// I implemented the bitwise instructions using the bitwise
		// operators at the top level, as all that does is instanciate
		// an array of gates for the inputs; there's no deeper 
		// implementation like there would be for A + B
		AND: begin
			result_temp = A & B;
			Z = !(|result_temp);
			N = result_temp[15];
		end
		
		OR: begin
			result_temp = A | B;
			Z = !(|result_temp);
			N = result_temp[15];
		end
		
		XOR: begin
			result_temp = A ^ B;
			Z = !(|result_temp);
			N = result_temp[15];
		end
		
		LSL: begin
			result_temp = shift_out;
			Z = !(|result_temp);
			N = result_temp[15];
		end
		
		NOP: begin end// do nothing
		
		default: $display($time, " command not found");
							
	endcase
end

// sign extend the result
assign result = {{16{result_temp[15]}}, result_temp};

endmodule
