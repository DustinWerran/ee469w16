module SRAM32(DATA, ADDRESS, CLK, nWE, OE);
	
//----BI-DIRECTIONAL PORT----//
inout [31:0] DATA;

//---------INPUTS---------//
input [10:0] ADDRESS;
input CLK, nWE, OE;


//-----MEMORY REGISTER---------//
reg [15:0] MEMORY [127:0];


assign DATA = OE ? {MEMORY[ADDRESS + 1], MEMORY[ADDRESS]} : 32'bz;
//-------READING--------//

always @(posedge CLK) begin
	//-----WRITING----------//
	if (!nWE && !OE) begin
		{MEMORY[ADDRESS + 1], MEMORY[ADDRESS]} <= DATA;
   end
end

initial
begin
$readmemb("../source/datamem.dat.txt", MEMORY);
end

endmodule


/*
module SRAM32_tb();
   
localparam half_clk = 10;

reg [10:0] ADDRESS;
reg CLK, nWE, OE, RST;

wire [31:0] DATA;

SRAM32 uut (.DATA(DATA), .ADDRESS(ADDRESS), .CLK(CLK), .nWE(nWE), .OE(OE), .RST(RST));

reg [31:0] DATAIN;

assign DATA = OE ? 32'bZ : DATAIN;

always #(2*half_clk)
   CLK = !CLK;

initial begin 
   DATAIN <= 32'b0;
   ADDRESS <= 11'b0;
   CLK <= 1'b0;
   nWE <= 1'b1;
   OE <= 1'b0;
   RST <= 1'b0;
end

initial begin
   @(posedge CLK);
   RST <= 1'b1;
   @(posedge CLK);
   RST <= 1'b0;
   @(posedge CLK);
   nWE <= 1'b0;
   DATAIN <= 32'hFFFF0000;
   @(posedge CLK);
   DATAIN <= 32'h0000FFFF;
   ADDRESS <= 11'b00000111111;
   @(posedge CLK);
   DATAIN <= 32'h0F0F0F0F;
   ADDRESS <= 11'b11111000000;
   @(posedge CLK);
   @(posedge CLK);
   nWE <= 1'b1;
   ADDRESS <= 11'b00000000000;
   @(posedge CLK);
   DATAIN <= 32'h77777777;
   @(posedge CLK);
   OE <= 1'b1;
   ADDRESS <= 11'b00000111111;
   @(posedge CLK);
   ADDRESS <= 11'b11111000000;
   @(posedge CLK);
   @(posedge CLK);
   @(posedge CLK);
   $stop;
end

endmodule*/
               
      
   
   
   
   
   
   