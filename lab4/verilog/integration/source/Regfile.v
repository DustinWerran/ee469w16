module Regfile(
	output [31:0] Reg1ReadData, Reg2ReadData,
	input [4:0] Reg1ReadSelect, Reg2ReadSelect,
	input [4:0] WriteRegSelect,
	input WriteEnable,
	input [31:0] WriteRegData,
	input clock,
	input reset
);

wire [31:0] Reg1ReadLines, Reg2ReadLines;
wire [30:0] RegWriteLines_pre, RegWriteLines;  // one less
wire [30:0] buffered_clocks; // one less

Decoder5to32 reg1decode (Reg1ReadLines, Reg1ReadSelect);
Decoder5to32 reg2decode (Reg2ReadLines, Reg2ReadSelect);
Decoder5to32 writedecode (RegWriteLines_pre, WriteRegSelect);

genvar i, j;

// generate the AND gates for write enable on all the write lines
generate
	for (i = 0; i < 31; i = i + 1) begin : write_enable
		and(RegWriteLines[i], RegWriteLines_pre[i], WriteEnable);
	end
endgenerate

// buffer the otherwise-enormous fanout
generate
	for (i = 0; i < 31; i = i + 1) begin : clock_buffers
		buf(buffered_clocks[i], clock);
	end
endgenerate

// generate all but the last register
generate
	for (i = 0; i < 31; i = i + 1) begin : rows
		for (j = 0; j < 32; j = j + 1) begin : columns
			Bitcell eachbit (
				.r1(Reg1ReadData[j]),
				.r2(Reg2ReadData[j]),
				.re1(Reg1ReadLines[i]),
				.re2(Reg2ReadLines[i]),
				.w(WriteRegData[j]),
				.we(RegWriteLines[i]),
				.clk(clock),
				.rst(reset)
			);
		end
	end
endgenerate

// generate the last register, which is always set to 0
generate
	for (i = 0; i < 32; i = i + 1) begin : zero_reg
		Bitcell eachbit (
			.r1(Reg1ReadData[i]),
			.r2(Reg2ReadData[i]),
			.re1(Reg1ReadLines[31]),
			.re2(Reg2ReadLines[31]),
			.w(),
			.we(),
			.clk(),
			.rst(reset)
		);
	end
endgenerate

endmodule
