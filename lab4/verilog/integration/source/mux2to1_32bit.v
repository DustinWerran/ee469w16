module mux2to1_32bit (in_0, in_1, Out, Sel);
	input [31:0] in_0, in_1;
	input Sel;
	output reg [31:0] Out;
	
	always @(*) begin
		if (Sel == 0) begin
			Out = in_0;
		end else begin
			Out = in_1;
		end
	end
	
endmodule
	