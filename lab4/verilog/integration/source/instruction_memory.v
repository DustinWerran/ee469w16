`timescale 1ns/1ps

module instruction_memory(instruction, address);

input [6:0] address;
output reg [31:0] instruction;
reg [31:0] instrmem [63:0];

always @(address)
begin
	instruction = instrmem[address];
end

initial
begin
	$readmemb("../source/instr.dat.txt", instrmem);
end

endmodule