module integration_single_cycle(
	input CLOCK_50, 
	input [3:0] KEY
);
	
	wire [31:0] im_read_data;
	wire [31:0] data_out;
	wire z_flag, n_flag, c_flag, v_flag;
	wire [2:0] ALU_ctl;
	wire [31:0] im_read_address;
	wire data_mux_ctl;
	wire [31:0] alu_op0_from_id, alu_op1_from_id;
	wire sel_alu_op0, sel_alu_op1;
	wire [4:0] rf_rd_addr_0, rf_rd_addr_1, rf_wr_addr;
	wire rf_wr_en;
	wire nWE, OE;
	wire [31:0] bus_b, bus_a;
	wire [31:0] sram_out, sram_write;
	wire [31:0] alu_result;
	wire [31:0] alu_a, alu_b;
	wire [31:0] alu_op0_id_extend, alu_op1_id_extend;
	
	assign sram_out = OE ? 32'bZ : bus_b;
	
	control mycontrol(
		// inputs
		.clk(CLOCK_50),
		.rst(!KEY[0]),
		.im_read_data(im_read_data),
		.data_out(data_out),
		.z_flag(z_flag),
		.n_flag(n_flag),
		.c_flag(c_flag),
		.v_flag(v_flag),
		// outputs
		.ALU_ctl(ALU_ctl),
		.im_read_address(im_read_address),
		.data_mux_ctl(data_mux_ctl),
		.alu_op0_from_id(alu_op0_from_id),
		.alu_op1_from_id(alu_op1_from_id),
		.sel_alu_op0(sel_alu_op0),
		.sel_alu_op1(sel_alu_op1),
		.rf_rd_addr_0(rf_rd_addr_0),
		.rf_rd_addr_1(rf_rd_addr_1),
		.rf_wr_addr(rf_wr_addr),
		.rf_wr_en(rf_wr_en),
		.nWE(nWE),
		.OE(OE)
	);
	
	instruction_memory im(
		.instruction(im_read_data),
		.address(im_read_address[6:0])
	);
	
	Regfile rf(
		.Reg1ReadData(bus_a), //rf outputA goes to ALU
		.Reg2ReadData(bus_b), //rf outputB
		.Reg1ReadSelect(rf_rd_addr_0),
		.Reg2ReadSelect(rf_rd_addr_1),
		.WriteRegSelect(rf_wr_addr),
		.WriteEnable(rf_wr_en),
		.WriteRegData(data_out), //output from D sel mux
		.clock(CLOCK_50),
		.reset(!KEY[0])
	);
	
	alu myalu(
		.A(alu_a), //input to ALU comes from regfile 1 read data, see sel_alu_op0
		.B(alu_b), //input to ALU comes from regfile 2 read data, see sel_alu_op1
		.ctl(ALU_ctl),
		.result(alu_result), //output from ALU, goes to Dsel and data memory
		.Z(z_flag),
		.N(n_flag),
		.C(c_flag),
		.V(v_flag)
	);
	
	SRAM32 dm(
		.DATA(sram_out), //bus b if writing, otherwise output to dsel
		.ADDRESS(alu_result), //output from ALU?
		.CLK(CLOCK_50),
		.nWE(nWE),
		.OE(OE)
	);
	
	// muxes for datapath control
	
	mux2to1_32bit Dsel(
		.in_0(alu_result),
		.in_1(sram_out),
		.Out(data_out),
		.Sel(data_mux_ctl)
	);
	
	mux2to1_32bit Asel(
		.in_0(bus_a),
		.in_1(alu_op0_from_id),
		.Out(alu_a),
		.Sel(sel_alu_op0)
	);
	
	mux2to1_32bit Bsel(
		.in_0(bus_b),
		.in_1(alu_op1_from_id),
		.Out(alu_b),
		.Sel(sel_alu_op1)
	);
	
endmodule

