`timescale 1ns/1ps

module integration_single_cycle_tb();

wire CLOCK_50;
reg [3:0] KEY;

reg clk;

integration_single_cycle uut (
	.CLOCK_50(CLOCK_50),
	.KEY(KEY)
);

assign CLOCK_50 = clk;

parameter half_clk = 2.5;

always #2.5
	clk = ~clk;
	
initial begin
	clk <= 1'b1;
	KEY <= 4'b1111;
end

initial begin
	@(posedge clk);
	KEY[0] <= 1'b0;
	@(posedge clk);
	KEY[0] <= 1'b1;
	forever
		@(posedge clk);
end

endmodule
