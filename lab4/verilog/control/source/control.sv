module control (

	input clk,
	input rst,
	
	input [31:0] im_read_data,  // data from im
	
	input [31:0] data_out,  // output of data mux
	
	input z_flag, n_flag, c_flag, v_flag,
	
	output reg [2:0] ALU_ctl,  // function select
	
	output [31:0] im_read_address,  // address to im, note: wire
	
	output reg data_mux_ctl,  // D sel
	
	output reg [15:0] alu_op0_from_id,  // constant
	output reg [15:0] alu_op1_from_id,  // constant
	
	output reg sel_alu_op0,  // controls ALU input mux
	output reg sel_alu_op1,  // controls ALU input mux
	
	output reg [4:0] rf_rd_addr_0,  // A adx
	output reg [4:0] rf_rd_addr_1,  // B adx
	output reg [4:0] rf_wr_addr,  // Reg Sel
	output reg rf_wr_en,  // RW
	
	output reg nWE,  // data mem write enable
	output reg OE  // data mem output enable
	
);


localparam NOP    = 32'b11111111000000000000000000000000,
			  ADD    = 32'b10001011000?????????????????????,
			  SUB    = 32'b11001011000?????????????????????,
			  AND    = 32'b10001010000?????????????????????,
			  ORR    = 32'b10101010000?????????????????????,
			  EOR    = 32'b11001010000?????????????????????,
			  LSL    = 32'b11010011011?????????????????????,
			  LDURSW = 32'b10111000100?????????????????????,
			  STURW  = 32'b10111000000?????????????????????,
			  B      = 32'b000101??????????????????????????,
			  BR     = 32'b11010110000?????????????????????,
			  B_cond = 32'b01010100????????????????????????;

localparam EQ     = 32'b01010100000?????????????????????,
			  NE     = 32'b01010100001?????????????????????,
			  LT     = 32'b01010100010?????????????????????,
			  LE     = 32'b01010100011?????????????????????,
			  GT     = 32'b01010100100?????????????????????,
			  GE     = 32'b01010100101?????????????????????,
			  MI     = 32'b01010100110?????????????????????,
			  PL     = 32'b01010100111?????????????????????;

reg [31:0] pc;  // program counter
reg [31:0] pc_comb;  // program counter combinational logic
reg [3:0] FLAGS;  // flag register
reg [3:0] FLAGS_comb;  // flag register combinational logic

assign im_read_address = pc;  // read from address pointed to by pc


always_ff @(posedge clk or posedge rst) begin
	if (rst) begin
		pc <= 32'b0;
		FLAGS <= 4'b0;
	end
	else begin
		pc <= pc_comb;
		FLAGS <= FLAGS_comb;
	end
end


always_comb begin
	
	pc_comb = pc + 1;
	FLAGS_comb = 4'bx;
	
	ALU_ctl = 3'b0;  // nop
	data_mux_ctl = 1'bx;
	alu_op0_from_id = 16'bx;
	alu_op1_from_id = 16'bx;
	sel_alu_op0 = 1'b0;
	sel_alu_op1 = 1'b0;
	rf_rd_addr_0 = 5'bx;
	rf_rd_addr_1 = 5'bx;
	rf_wr_addr = 5'bx;
	rf_wr_en = 1'b0;
	nWE = 1'b1;  // low
	OE = 1'b0;
	
	unique casez (im_read_data)
		
		ADD: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
		
		end
		
		SUB: begin
		
			ALU_ctl = 3'b010;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;

		end
		
		AND: begin
		
			ALU_ctl = 3'b011;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;

		end
		
		ORR: begin
		
			ALU_ctl = 3'b100;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;

		end
		
		EOR: begin
		
			ALU_ctl = 3'b101;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			rf_rd_addr_1 = im_read_data[20:16];
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;

		end
		
		LSL: begin
		
			ALU_ctl = 3'b110;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			alu_op1_from_id = im_read_data[15:10];
			sel_alu_op1 = 1'b1;
			
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;

		end
		
		LDURSW: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctrl = 1'b1;
			OE = 1'b1;
			
			rf_rd_addr_0 = im_read_data[9:5];
			alu_op1_from_id = im_read_data[20:12];
			sel_alu_op1 = 1'b1;
			
			rf_wr_addr = im_read_data[4:0];
			rf_wr_en = 1'b1;
			
		end
		
		STURW: begin
		
			ALU_ctl = 3'b001;
			
			nWE = 1'b0;
			
			rf_rd_addr_0 = im_read_data[9:5];
			alu_op1_from_id = im_read_data[20:12];
			sel_alu_op1 = 1'b1;
			
			rf_rd_addr_1 = im_read_data[4:0];
		
		end
		
		B: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctrl = 1'b0;
			
			alu_op0_from_id = im_read_data[25:0];
			alu_op1_from_id = pc;
			sel_alu_op0 = 1'b1;
			sel_alu_op1 = 1'b1;
			
			pc_comb = data_out;	
		
		end
		
		BR: begin
		
			ALU_ctl = 3'b110;
			
			data_mux_ctrl = 1'b0;
			
			rf_rd_addr_0 = im_read_data[4:0];
			alu_op1_from_id = 32'b0;  // shift zero
			sel_alu_op1 = 1'b1;
			
			pc_comb = data_out;	
		
		end
		
		B_cond: begin
		
			ALU_ctl = 3'b001;
			
			data_mux_ctrl = 1'b0;
			
			alu_op0_from_id = im_read_data[25:0];
			alu_op1_from_id = pc;
			sel_alu_op0 = 1'b1;
			sel_alu_op1 = 1'b1;
		
			unique casez (im_read_data)
			
				EQ: begin  // not yet implemented
				end
				
				NE: begin  // not yet implemented
				end
				
				LT: begin  // not yet implemented
				end
				
				LE: begin  // not yet implemented
				end
				
				GT: if (FLAGS[3] == 1'b0 && FLAGS[2] == FLAGS[0]) pc_comb = data_out;
				
				GE: begin  // not yet implemented
				end
				
				MI: begin  // not yet implemented
				end
				
				PL: begin  // not yet implemented
				end
				
			endcase
		end
		
		// NOP:  // do nothing
		
	endcase
end

endmodule
